﻿#pragma once
#ifndef SAT_COMMON_LOGGER_H
#define SAT_COMMON_LOGGER_H

#include <boost/lexical_cast.hpp>
#include <iostream>

#if defined(LOGGING)
#include <boost/thread.hpp>

class Locker
{
public:
	static boost::mutex& Get()
	{
		static boost::mutex guard;
		return guard;
	}
private:
	Locker();
	Locker(const Locker&);
	Locker& operator=(const Locker&);
};

#endif

#define LOG_STR(what) boost::lexical_cast<std::string>(what)

#if defined(LOG_ERROR_LEVEL) && defined(LOGGING)
	#define LOG_ERROR(What) \
		Locker::Get().lock(); \
								std::cerr << "(ERROR) "  << \
								__FILE__ << "(" \
								<< __LINE__ << ") (" \
								<< __FUNCTION__ << ") " \
								<< What << std::endl; \
		Locker::Get().unlock();
#else
	#define LOG_ERROR(What)
#endif

#if defined(LOG_WARNING_LEVEL) && defined(LOGGING)
	#define LOG_WARNING(What) \
		Locker::Get().lock(); \
								std::cerr << "(WARNING) " \
								<< __FILE__ << "(" \
								<< __LINE__ << ") (" \
								<< __FUNCTION__ << ") " \
								<< What << std::endl; \
		Locker::Get().unlock();
#else
	#define LOG_WARNING(What)
#endif

#if defined(LOG_DEBUG_LEVEL) && defined(LOGGING)
	#define LOG_DEBUG(What) \
		Locker::Get().lock(); \
								std::cerr << "(DEBUG) " \
								<< __FILE__ << "(" \
								<< __LINE__ << ") (" \
								<< __FUNCTION__ << ") " \
								<< What << std::endl; \
		Locker::Get().unlock();
#else
	#define LOG_DEBUG(What)
#endif

#if defined(LOG_INFO_LEVEL) && defined(LOGGING)
	#define LOG_INFO(What) \
		Locker::Get().lock(); \
		std::cout << "(INFO) (" << __FUNCTION__ << ") " << What << std::endl; \
		Locker::Get().unlock();

#else
	#define LOG_INFO(What)
#endif

#define LOG_VERIFY_THROW(What, LogLevel, Exception) \
	if (!(What)) \
	{ \
		LogLevel("!"#What); \
		throw Exception("!"#What); \
	} \


#define LOG_VERIFY_RETURN(What, LogLevel, Value) \
	if (!(What)) \
	{ \
		LogLevel("!"#What); \
		return Value; \
	} \

#define LOG_VARIABLE(Value, LogLevel) \
	LogLevel(#Value + std::string(" = ") + LOG_STR(Value));

#endif // SAT_COMMON_LOGGER_H
