TEMPLATE = app
TARGET = Transalg

include(../../Mk/Common/common.pri)

CONFIG += console

CONFIG(debug, debug|release) {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$DEBUG_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$DEST_FOLDER/$$DEBUG_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
} else {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$RELEASE_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$DEST_FOLDER/$$RELEASE_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
}

# Библиотеки, от которых зависим
LIBRARIES = \
	TransalgFormats \
	TransalgInterpreter \
	TransalgParser \
	EspressoLib \
	boost_program_options \
	boost_system \
	boost_thread
	
for(i, LIBRARIES) {
	LIBS += -l$${i}
}

# Дополнительные каталоги включения для данного проекта
INCLUDEPATH += . ..

SOURCES = Transalg.cpp

# Отладочная информация

#!build_pass:message("Config: $$CONFIG")

#!build_pass:message("Lib paths:")
#for(i, LIBS) {
#	!build_pass:message($${i})
#}

#!build_pass:message("Include paths:")
#for(i, INCLUDEPATH) {
#	!build_pass:message($${i})
#}
