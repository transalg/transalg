#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>

#include <boost/program_options.hpp>

//! Лексический анализатор ТА-программ
#include <TransalgParser/src/Lexer.h>

#include <TransalgParser/Parser.h>
#include <TransalgParser/Serializer.h>
#include <TransalgParser/ParseError.h>

#include <TransalgInterpreter/Interpreter.h>
#include <TransalgFormats/OutputGenerator.h>
#include <TransalgFormats/EncodeFactory.h>

void GetProgramText(const std::string& filename, std::string& program_text);

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;

	po::options_description options("Usage: Transalg_TestApp [options]\nAvailable options");
	options.add_options()
		("help,h", "display this help and exit")
		("input,i", po::value<std::string>(), "input file containing the TA-program")
		("output,o", po::value<std::string>(), "output file containing propositional encoding")
		("format,f", po::value<std::string>()->default_value("cnf"), "output format {cnf, ls}")
		// action - debug option
		("action,a", po::value<std::string>()->default_value("t"), 
			"action: 'l' - output program lexems, 's' - parse and output statements, 't' - translate program to boolean equations")
		("version,v", "output current program version")
		;

	try
	{
		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if(params.count("help"))
		{
			std::cout << options;
			return 0;
		}

		if(params.count("version"))
		{
			std::cout << "Version: 0.1.0.0" << std::endl;
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
			return 0;
		}

		if(!params.count("input"))
		{
			throw std::invalid_argument("main: input file is not defined!");
		}

		if(!params.count("output"))
		{
			throw std::invalid_argument("main: output file is not defined!");
		}

		const std::string input_file = params["input"].as<std::string>();
		const std::string output_file = params["output"].as<std::string>();
		const std::string output_format = params["format"].as<std::string>();
		const std::string action = params["action"].as<std::string>();

		//! Вывести все токены в файл
		if(action.find('l') != std::string::npos)
		{
			std::ofstream out(output_file.c_str(), std::ios::out);
			if(!out.is_open())
				throw std::runtime_error(std::string("Can't open file: ") + output_file);

			//! Прочитать весь текст программы из файла в строку
			std::string program_text;
			GetProgramText(input_file, program_text);
			
			//! Выполнить лексический разбор текста программы
			Transalg::TokenStream tokens;
			Transalg::Lexer lexer;
			lexer.ParseTokens(program_text, tokens);
			Transalg::WriteOut(out, tokens);
		}

		//! Вывести в файл содержимое инструкций
		if(action.find('s') != std::string::npos)
		{
			std::ofstream out(output_file.c_str(), std::ios::out);
			if(!out.is_open())
				throw std::runtime_error(std::string("Can't open file: ") + output_file);

			Transalg::Parser parser;
			Transalg::StatementBlockPtr program_block_ptr = parser.ParseProgram(input_file);
			Transalg::Serializer::SerializeStatementBlock(out, *program_block_ptr);
		}

		//! Транслировать программу в систему булевых уравнений
		if(action.find('t') != std::string::npos)
		{
			Transalg::Parser parser;
			Transalg::StatementBlockPtr program_block = parser.ParseProgram(input_file);

			Transalg::EncodeFactoryPtr encode = boost::make_shared<Transalg::EncodeFactory>();
			Transalg::Interpreter interpreter;
			if(!interpreter.TranslateProgram(program_block, encode))
			{
				throw std::runtime_error("main: program translate is failed!");
			}

			std::cout << "Input   encode variable count: " << encode->input_vars.size() << std::endl;
			std::cout << "Tseitin encode variable count: " << encode->tseitin_vars.size() << std::endl;
			std::cout << "Output  encode variable count: " << encode->output_vars.size() << std::endl;
			std::cout << "Constraints count: " << encode->constraints.size() << std::endl;

			Transalg::OutputFormat format = Transalg::OutputFormatCnf;
			if(output_format == "ls")
				format = Transalg::OutputFormatLs;

			{
				std::ofstream err_log("error_log.txt", std::ios::out);
				encode->CheckEncode(err_log);
				encode->RenumberingVariables();
			}

			Transalg::OutputGenerator generator;
			generator.AddEncode(encode->input_vars);
			generator.AddEncode(encode->tseitin_vars);
			generator.AddEncode(encode->output_vars);
			generator.AddEncode(encode->constraints);
			generator.Generate(output_file, format);
		}

	}
	catch (const Transalg::ParseError& e)
	{
		std::cerr << "PARSE ERROR: " << e.what() << std::endl;
		return 1;
	}
	catch (const std::exception& e)
	{
		std::cerr << "ERROR: " << e.what() << std::endl;
		return 1;
	}

	return 0;
}

void GetProgramText(const std::string& filename, std::string& program_text)
{
	std::ifstream in(filename.c_str(), std::ios::in);
	if(!in)
	{
		throw std::runtime_error(std::string("Can't open file: ") + filename);
	}
	std::stringstream ss;
	ss << in.rdbuf();
	program_text = ss.str();
}
