#pragma once

#include <TransalgParser/Expression.h>
#include <TransalgParser/StatementVarDefinition.h>
#include <TransalgParser/StatementArrayDefinition.h>
#include <TransalgParser/StatementFunctionDefinition.h>
#include <TransalgParser/StatementBlock.h>
#include <TransalgParser/StatementIf.h>
#include <TransalgParser/StatementFor.h>
#include <TransalgParser/StatementReturn.h>
#include <TransalgParser/StatementTable.h>

#include "DataObject.h"

namespace Transalg
{

//! Связь между переменной ТА-программы и логической формулой
typedef std::pair<BitPtr, LogicFormulaPtr> Suggest;
typedef std::vector<Suggest> SuggestContainer;

class IInterpreterImpl
{
public:
	
	virtual ~IInterpreterImpl() {}

	virtual DataObjectVarPtr ExecStatementVarDefinition(const StatementVarDefinitionPtr& stmt) = 0;

	virtual DataObjectArrayPtr ExecStatementArrayDefinition(const StatementArrayDefinitionPtr& stmt) = 0;
	
	virtual DataObjectFunctionPtr ExecStatementFunctionDefinition(const StatementFunctionDefinitionPtr& stmt) = 0;

	virtual void ExecStatement(const StatementPtr& stmt, VoidPtr& func_result) = 0;

	virtual void ExecStatementBlock(const StatementBlockPtr& block, VoidPtr& func_result) = 0;

	virtual void ExecStatementIf(const StatementIfPtr& stmt, VoidPtr& func_result) = 0;

	virtual void ExecStatementFor(const StatementForPtr& stmt, VoidPtr& func_result) = 0;

	virtual VoidPtr ExecFunctionCall(const DataObjectFunctionPtr& func_ptr, const VoidContainer& arguments) = 0;

	// return VoidPtr ?
	virtual void ExecStatementTable(const StatementTablePtr& table) = 0;

	virtual bool IsBranchMode() const = 0;

	virtual bool IsLocalVar(const BitPtr& var) const = 0;

	virtual SuggestContainer* GetSuggestBranchVars() const = 0;

	virtual SuggestContainer::const_iterator FindVarInSuggest(const BitPtr& var, const SuggestContainer& var_container) const = 0;

	virtual SuggestContainer::iterator FindVarInSuggest(const BitPtr& var, SuggestContainer& var_container) const = 0;
};

typedef boost::shared_ptr<IInterpreterImpl> IInterpreterImplPtr;

} // namespace Transalg
