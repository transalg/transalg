#pragma once

namespace Transalg
{

class Void;
class Integer;
class Bit;
class BitVector;

typedef boost::shared_ptr<Void> VoidPtr;
typedef boost::shared_ptr<Integer> IntegerPtr;
typedef boost::shared_ptr<Bit> BitPtr;
typedef boost::shared_ptr<BitVector> BitVectorPtr;

typedef std::vector<VoidPtr> VoidContainer;
typedef std::vector<IntegerPtr> IntegerContainer;
typedef std::vector<BitPtr> BitContainer;
typedef std::vector<BitVectorPtr> BitVectorContainer;

typedef boost::shared_ptr<VoidContainer> VoidContainerPtr;
typedef boost::shared_ptr<IntegerContainer> IntegerContainerPtr;
typedef boost::shared_ptr<BitContainer> BitContainerPtr;
typedef boost::shared_ptr<BitVectorContainer> BitVectorContainerPtr;

IntegerPtr toInt(VoidPtr x);

BitPtr toBit(VoidPtr x);

BitVectorPtr toBitVector(VoidPtr x);

void PrintData(std::ostream& out, VoidPtr x);

} // namespace Transalg
