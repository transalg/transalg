#include "stdafx.h"
#include "DataTypeInteger.h"
#include "DataTypeBit.h"
#include "SemanticLibrary.h"

namespace Transalg
{
namespace SemanticLib
{

int CircleLeftShift(int x, unsigned pos)
{
	const unsigned bits = 8 * sizeof(x);
	pos %= bits;
	return (x << pos)|(x >> (bits - pos));
}

int CircleRightShift(int x, unsigned pos)
{
	const unsigned bits = 8 * sizeof(x);
	pos %= bits;
	return (x >> pos)|(x << (bits - pos));
}

int SaveLeftShift(int x, unsigned pos)
{
	const unsigned bits = 8 * sizeof(x);
	//! "залипает" весь вектор
	if(pos >= bits) return x;
	//! маска на те биты, которые залипают
	unsigned mask = 0;
	for(unsigned i = 0; i < pos; i++)
		mask |= static_cast<int>(1) << i;
	return (x << pos) | (mask & x);
}

int SaveRightShift(int x, unsigned pos)
{
	const unsigned bits = 8 * sizeof(x);
	//! "залипает" весь вектор
	if(pos >= bits) return x;
	//! маска на те биты, которые залипают
	unsigned mask = 0;
	for(unsigned i = 1; i <= pos; i++)
		mask |= static_cast<int>(1) << (bits - i);
	return (x >> pos) | (mask & x);
}

std::string PrintBits(unsigned n)
{
	std::string s;
	const unsigned bits = 8 * sizeof(n);
	for(unsigned i = 0; i < bits; ++i)
		s.insert(0, ((n >> i) & 1 ? "1" : "0"));
	return s;
}

unsigned MajorBitPos(unsigned x)
{
	unsigned pos = 0;
	while(x)
	{
		x >>= 1;
		++pos;
	}
	return pos;
}

void RemoveLeadFalseBits(LogicFormulaContainer& vec)
{
	while(vec.size())
	{
		const LogicFormulaPtr& bit = vec.back();
		if(!bit || (bit->IsConst() && bit->GetValue().False()))
		{
			vec.resize(vec.size() - 1);
		}
		else
			break;
	}
	if(vec.size() == 0)
		vec.push_back(EncodeFactory::False());
}

BitVectorPtr multiply(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::multiply: argument - null pointer.");

	//! Первым операндом поставим самый короткий вектор - строк в матрице меньше чем столбцов, 
	//! что дает меньше промежуточных операций целочисленного сложения
	const BitVector& x = (vx->GetSize() < vy->GetSize() ? *vy : *vx);
	const BitVector& y = (vx->GetSize() < vy->GetSize() ? *vx : *vy);
	const std::size_t x_size(x.GetSize());
	const std::size_t y_size(y.GetSize());

	unsigned i, j;
	std::vector<LogicFormulaContainer> mult_matrix(y_size);
	for(i = 0; i < y_size; ++i)
	{
		mult_matrix[i].resize(x_size);
		for(j = 0; j < x_size; ++j)
		{
			mult_matrix[i][j] = EncodeFactory::And(x[j]->GetValue(), y[i]->GetValue());
		}
	}

	//! mult - пропозициональная кодировка целочисленного умножения
	LogicFormulaContainer mult(x_size + y_size);
	//! carry_bit, next_carry_bit - биты переноса
	LogicFormulaPtr carry_bit;
	LogicFormulaPtr next_carry_bit;
	//! Копируем 1-ю строку матрицы
	for(j = 0; j < x_size; ++j)
	{
		mult[j] = mult_matrix[0][j];
	}
	for(i = 1; i < y_size; ++i)
	{
		// fixme: можно сразу создавать новую переменную, но пока так оставляем, чтобы сравнивать с предыдущей версией
		carry_bit = EncodeFactory::And(mult[i], mult_matrix[i][0]);
		mult[i] = encode.CreateVarIfFunction(EncodeFactory::Xor(mult[i], mult_matrix[i][0]));
		carry_bit = encode.CreateVarIfFunction(carry_bit);
		for(j = i + 1; j < x_size + i - 1; ++j)
		{
			next_carry_bit = encode.CreateVarIfFunction(EncodeFactory::Majority(mult[j], mult_matrix[i][j - i], carry_bit));
			mult[j] = encode.CreateVarIfFunction(EncodeFactory::Xor(mult[j], EncodeFactory::Xor(mult_matrix[i][j - i], carry_bit)));
			carry_bit = next_carry_bit;
		}
		mult[x_size + i - 1] = encode.CreateVarIfFunction(EncodeFactory::Xor(EncodeFactory::Xor(mult_matrix[i][x_size - 1], carry_bit), mult[x_size + i - 1]));
		mult[x_size + i] = EncodeFactory::And(mult_matrix[i][x_size - 1], carry_bit);
	}

	//! Удаляем ведущие нули
	RemoveLeadFalseBits(mult);
	return boost::make_shared<BitVector>(mult);
}

BitVectorPtr multiply(BitVectorPtr vx, BitVectorPtr vy, std::size_t bit_size, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::multiply: argument - null pointer.");

	if(bit_size == 0)
		throw std::invalid_argument("SemanticLib::multiply: bit size of result == 0.");

	if(bit_size == 1)
	{
		LogicFormulaPtr vx_formula = vx->GetBit(0)->GetValue();
		LogicFormulaPtr vy_formula = vy->GetBit(0)->GetValue();
		LogicFormulaPtr bit = encode.CreateVarIfFunction(EncodeFactory::And(vx_formula, vy_formula));
		LogicFormulaContainer vec;
		vec.push_back(bit);
		return boost::make_shared<BitVector>(vec);
	}

	//! Первым операндом поставим самый короткий вектор - строк в матрице меньше чем столбцов, 
	//! что дает меньше промежуточных операций целочисленного сложения
	if(vx->GetSize() < vy->GetSize())
	{
		std::swap(vx, vy);
	}
	const BitVector& x = *vx;
	const BitVector& y = *vy;
	const std::size_t x_size = x.GetSize();
	const std::size_t y_size = y.GetSize();
	
	unsigned i, j;
	std::vector<LogicFormulaContainer> mult_matrix(y_size);
	for(i = 0; i < y_size; ++i)
	{
		mult_matrix[i].resize(x_size);
		for(j = 0; j < x_size; ++j)
		{
			mult_matrix[i][j] = EncodeFactory::And(x[j]->GetValue(), y[i]->GetValue());
		}
	}

	const std::size_t mult_size = std::min(bit_size, x_size + y_size);
	LogicFormulaContainer mult(mult_size);
	LogicFormulaPtr carry_bit;
	LogicFormulaPtr next_carry_bit;
	for(j = 0; j < std::min(x_size, mult_size); ++j)
	{
		mult[j] = mult_matrix[0][j];
	}
	for(i = 1; i < std::min(y_size, mult_size); ++i)
	{
		if(i + 1 < mult_size)
		{
			carry_bit = encode.CreateVarIfFunction(EncodeFactory::And(mult[i], mult_matrix[i][0]));
		}
		mult[i] = encode.CreateVarIfFunction(EncodeFactory::Xor(mult[i], mult_matrix[i][0]));
		for(j = i + 1; (j < x_size + i - 1) && (j < mult_size); ++j)
		{
			if(j < mult_size - 1)
			{
				next_carry_bit = EncodeFactory::Majority(mult[j], mult_matrix[i][j - i], carry_bit);
				next_carry_bit = encode.CreateVarIfFunction(next_carry_bit);
			}	
			mult[j] = encode.CreateVarIfFunction(EncodeFactory::Xor(mult[j], EncodeFactory::Xor(mult_matrix[i][j - i], carry_bit)));
			if(j < mult_size - 1)
			{
				carry_bit = next_carry_bit;
			}
		}

		if(x_size + i - 1 < mult_size)
		{
			mult[x_size + i - 1] = encode.CreateVarIfFunction(
				EncodeFactory::Xor(EncodeFactory::Xor(mult_matrix[i][x_size - 1], carry_bit), mult[x_size + i - 1]));
		}
		if(x_size + i < mult_size)
		{
			mult[x_size + i] = EncodeFactory::And(mult_matrix[i][x_size - 1], carry_bit);
		}
	}

	//! Удаляем старшие нулевые биты
	RemoveLeadFalseBits(mult);
	return boost::make_shared<BitVector>(mult);
}

BitVectorPtr sum(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::sum: argument - null pointer.");

	const BitVector& x = (vx->GetSize() < vy->GetSize() ? *vy : *vx); // big
	const BitVector& y = (vx->GetSize() < vy->GetSize() ? *vx : *vy); // few
	const std::size_t x_size(x.GetSize());
	const std::size_t y_size(y.GetSize());
	assert(x_size >= y_size);

	if(!x_size || !y_size)
		throw std::invalid_argument("SemanticLib::sum: size of operands == 0.");

	LogicFormulaPtr bit = EncodeFactory::Xor(x[0]->GetValue(), y[0]->GetValue());
	//! Биты переноса
	LogicFormulaContainer carry_bits(x_size);
	//! Результат суммирования
	LogicFormulaContainer result(x_size + 1);
	result[0] = encode.CreateVarIfFunction(bit);
	carry_bits[0] = EncodeFactory::And(x[0]->GetValue(), y[0]->GetValue());

	unsigned i(0);
	for(i = 1; i < y_size; ++i)
	{
		bit = EncodeFactory::Xor(EncodeFactory::Xor(x[i]->GetValue(), y[i]->GetValue()), carry_bits[i - 1]);
		result[i] = encode.CreateVarIfFunction(bit);
		carry_bits[i] = EncodeFactory::Majority(x[i]->GetValue(), y[i]->GetValue(), carry_bits[i - 1]);
		carry_bits[i] = encode.CreateVarIfFunction(carry_bits[i]);
	}

	for(i = y_size; i < x_size; ++i)
	{
		bit = EncodeFactory::Xor(x[i]->GetValue(), carry_bits[i - 1]);
		result[i] = encode.CreateVarIfFunction(bit);
		carry_bits[i] = EncodeFactory::And(x[i]->GetValue(), carry_bits[i - 1]);
		carry_bits[i] = encode.CreateVarIfFunction(carry_bits[i]);
	}

	const unsigned e = x_size - 1;
	result[x_size] = encode.CreateVarIfFunction(carry_bits[e]);

	//! Удаляем старшие нулевые биты
	RemoveLeadFalseBits(result);
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr sum(BitVectorPtr vx, BitVectorPtr vy, std::size_t bit_size, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::sum: argument - null pointer.");

	if(bit_size == 0)
		throw std::invalid_argument("SemanticLib::sum: bit size of result == 0.");

	if(bit_size == 1)
	{
		LogicFormulaPtr vx_formula = vx->GetBit(0)->GetValue();
		LogicFormulaPtr vy_formula = vy->GetBit(0)->GetValue();
		LogicFormulaContainer vec;
		vec.push_back(encode.CreateVarIfFunction(EncodeFactory::Xor(vx_formula, vy_formula)));
		return boost::make_shared<BitVector>(vec);
	}

	const BitVector& x = (vx->GetSize() < vy->GetSize() ? *vy : *vx); // big
	const BitVector& y = (vx->GetSize() < vy->GetSize() ? *vx : *vy); // few
	const std::size_t x_size(x.GetSize());
	const std::size_t y_size(y.GetSize());
	assert(x_size >= y_size);

	if(!x_size || !y_size)
		throw std::invalid_argument("SemanticLib::sum: size of operands == 0.");

	const std::size_t result_size = std::min(bit_size, x_size + 1);
	assert(result_size > 0);
	//! Биты переноса
	LogicFormulaContainer carry_bits(result_size - 1);
	//! Результат сложения
	LogicFormulaContainer result(result_size);
	LogicFormulaPtr bit = EncodeFactory::Xor(x[0]->GetValue(), y[0]->GetValue());
	result[0] = encode.CreateVarIfFunction(bit);
	carry_bits[0] = EncodeFactory::And(x[0]->GetValue(), y[0]->GetValue());

	std::size_t i(0);
	for(i = 1; i < std::min(result_size, y_size); ++i)
	{
		bit = EncodeFactory::Xor(EncodeFactory::Xor(x[i]->GetValue(), y[i]->GetValue()), carry_bits[i - 1]);
		result[i] = encode.CreateVarIfFunction(bit);
		if(i < result_size - 1)
		{
			bit = EncodeFactory::Majority(x[i]->GetValue(), y[i]->GetValue(), carry_bits[i - 1]);
			carry_bits[i] = encode.CreateVarIfFunction(bit);
		}
	}

	for(i = std::min(result_size, y_size); i < std::min(result_size, x_size); ++i)
	{
		bit = EncodeFactory::Xor(x[i]->GetValue(), carry_bits[i - 1]);
		result[i] = encode.CreateVarIfFunction(bit);
		if(i < result_size - 1)
		{
			//! Для битов переноса заводим новые переменные, т.к. иначе при большом числе итераций "распухает" уравнение
			bit = EncodeFactory::And(x[i]->GetValue(), carry_bits[i - 1]);
			carry_bits[i] = encode.CreateVarIfFunction(bit);
		}
	}

	if(result_size > x_size)
	{
		const unsigned e = x_size - 1;
		result[x_size] = encode.CreateVarIfFunction(carry_bits[e]);
	}
	//! Удаляем старшие нулевые биты
	RemoveLeadFalseBits(result);
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr sub(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	if(!x || !y)
		throw std::invalid_argument("SemanticLib::sub: argument - null pointer.");
	if(!y->IsConst() || !y->GetSize())
		throw std::invalid_argument("SemanticLib::sub: second operand must be a const integer.");
	if(x->GetSize() < y->GetSize())
		throw std::invalid_argument("SemanticLib::sub: size of the first operand is less than size of the second operand.");

	const std::size_t x_size(x->GetSize());
	const std::size_t y_size(y->GetSize());
	LogicFormulaContainer result(x_size);
	LogicFormulaContainer tmp(x_size);
	for(int i = static_cast<int>(y_size) - 1; i >= 0; --i)
	{
		const LogicFormulaPtr bit = y->GetBit(i)->GetValue();
		if(bit->GetValue() == Transalg::LogicValueTrue)
		{
			tmp[i] = EncodeFactory::Not(result[i]);
			for(int j = i + 1; j < static_cast<int>(x_size); ++j)
			{
				LogicFormulaPtr formula;
				for(int k = i; k <= j; ++k)
					formula = EncodeFactory::And(formula, EncodeFactory::Not(result[k]));
				// формируем промежуточный результат вычитания
				for(int k = i; k < j; ++k)
					formula = EncodeFactory::Or(formula, EncodeFactory::And(result[k], result[j]));
				tmp[j] = encode.CreateVarIfFunction(formula);
			}
			result = tmp;
		}
	}
	//! Удаляем старшие нулевые биты
	RemoveLeadFalseBits(result);
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr and_(BitVectorPtr vx, BitVectorPtr vy)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::and_: argument - null pointer.");

	const std::size_t result_size = std::min(vx->GetSize(), vy->GetSize());
	if(result_size == 0)
		throw std::invalid_argument("SemanticLib::and_: size of the result is zero.");

	const BitVector& x = *vx;
	const BitVector& y = *vy;

	LogicFormulaContainer result(result_size);
	for(unsigned i = 0; i < result_size; ++i)
	{
		result[i] = EncodeFactory::And(x[i]->GetValue(), y[i]->GetValue());
	}
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr and_(BitVectorPtr vx, LogicFormulaPtr y)
{
	if(!vx || !y)
		throw std::invalid_argument("SemanticLib::and_: argument - null pointer.");

	const std::size_t result_size = vx->GetSize();
	if(result_size == 0)
		throw std::invalid_argument("SemanticLib::and_: size of the result is zero.");

	const BitVector& x = *vx;
	LogicFormulaContainer result(result_size);
	for(unsigned i = 0; i < result_size; ++i)
	{
		result[i] = EncodeFactory::And(x[i]->GetValue(), y);
	}
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr or_(BitVectorPtr vx, BitVectorPtr vy)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::or_: argument - null pointer.");

	//! Гарантируем, что x->GetSize() >= y->GetSize()
	if(vx->GetSize() < vy->GetSize())
		std::swap(vx, vy);

	const BitVector& x = *vx;
	const BitVector& y = *vy;
	const std::size_t x_size = x.GetSize();
	const std::size_t y_size = y.GetSize();

	LogicFormulaContainer result(x_size);
	unsigned i;
	for(i = 0; i < y_size; ++i)
	{
		result[i] = EncodeFactory::Or(x[i]->GetValue(), y[i]->GetValue());
	}
	for(i = y_size; i < x_size; ++i)
	{
		result[i] = x[i]->GetValue();
	}
	if(result.size() == 0)
		throw std::invalid_argument("SemanticLib::or_: size of the result is zero.");
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr or_(BitVectorPtr vx, LogicFormulaPtr y)
{
	if(!vx || !y)
		throw std::invalid_argument("SemanticLib::or_: argument - null pointer.");

	const BitVector& x = *vx;
	const std::size_t result_size = x.GetSize();
	if(result_size == 0)
		throw std::invalid_argument("SemanticLib::or_: size of the result is zero.");

	LogicFormulaContainer result(result_size);
	for(unsigned i = 0; i < result_size; ++i)
	{
		result[i] = EncodeFactory::Or(x[i]->GetValue(), y);
	}
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr xor_(BitVectorPtr vx, BitVectorPtr vy)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::xor_: argument - null pointer.");

	//! Гарантируем, что vx->GetSize() >= vy->GetSize()
	if(vx->GetSize() < vy->GetSize())
		std::swap(vx, vy);

	const BitVector& x = *vx;
	const BitVector& y = *vy;
	const std::size_t x_size = x.GetSize();
	const std::size_t y_size = y.GetSize();

	LogicFormulaContainer result(x_size);
	unsigned i;
	for(i = 0; i < y_size; ++i)
	{
		result[i] = EncodeFactory::Xor(x[i]->GetValue(), y[i]->GetValue());
	}
	for(i = y_size; i < x_size; ++i)
	{
		result[i] = x[i]->GetValue();
	}
	if(result.size() == 0)
		throw std::invalid_argument("SemanticLib::xor_: size of the result is zero.");
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr xor_(BitVectorPtr vx, LogicFormulaPtr y)
{
	if(!vx || !y)
		throw std::invalid_argument("SemanticLib::xor_: argument - null pointer.");

	const BitVector& x = *vx;
	const std::size_t result_size = x.GetSize();
	if(result_size == 0)
		throw std::invalid_argument("SemanticLib::xor_: size of the result is zero.");

	LogicFormulaContainer result(result_size);
	for(unsigned i = 0; i < result_size; ++i)
	{
		result[i] = EncodeFactory::Xor(x[i]->GetValue(), y);
	}
	return boost::make_shared<BitVector>(result);
}

BitVectorPtr not_(BitVectorPtr vx)
{
	if(!vx)
		throw std::invalid_argument("SemanticLib::not_: argument - null pointer.");

	const BitVector& x = *vx;
	const std::size_t result_size = x.GetSize();
	if(result_size == 0)
		throw std::invalid_argument("SemanticLib::not_: size of the result is zero.");

	LogicFormulaContainer result(result_size);
	for(unsigned i = 0; i < result_size; ++i)
	{
		result[i] = EncodeFactory::Not(x[i]->GetValue());
	}
	return boost::make_shared<BitVector>(result);
}

// x > y - где x, y - произвольные булевы векторы
LogicFormulaPtr great(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::great: argument - null pointer.");

	LogicFormulaPtr equation;
	LogicFormulaPtr conj;
	const BitVector& x = *vx;
	const BitVector& y = *vy;
	const std::size_t x_size = x.GetSize();
	const std::size_t y_size = y.GetSize();
	const int start = std::min(x_size, y_size) - 1;
	for(int i = start; i >= 0; --i)
	{
		conj = EncodeFactory::And(x[i]->GetValue(), EncodeFactory::Not(y[i]->GetValue()));
		for(int j = start; j > i; --j)
		{
			conj = EncodeFactory::And(conj, EncodeFactory::Eq(x[j]->GetValue(), y[j]->GetValue()));
			conj = encode.CreateVarIfFunction(conj);
		}
		equation = encode.CreateVarIfFunction(EncodeFactory::Or(equation, conj));
	}
	conj.reset();
	if(x_size > y_size)
	{
		//! Хотя бы 1 из старших бит вектора x принимает значение 1
		for(unsigned i = y_size; i < x_size; ++i)
		{
			conj = EncodeFactory::Or(conj, x[i]->GetValue());
			conj = encode.CreateVarIfFunction(conj);
		}
		equation = encode.CreateVarIfFunction(EncodeFactory::Or(conj, equation));
	}
	else if(x_size < y_size)
	{
		//! Все старшие биты вектора y должны принимать значение 0
		for(unsigned i = x_size; i < y_size; ++i)
		{
			conj = EncodeFactory::And(conj, EncodeFactory::Not(y[i]->GetValue()));
			conj = encode.CreateVarIfFunction(conj);
		}
		equation = encode.CreateVarIfFunction(EncodeFactory::And(conj, equation));
	}
	return equation;
}

// Оптимизированный вариант для кодирования псевдобулевых неравенств (ДНФ)
// предполагается, что x - вектор булевых переменных, y - константный вектор
LogicFormulaPtr great_const(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::great_const: argument - null pointer.");
	if(!vy->IsConst())
		throw std::invalid_argument("SemanticLib::great_const: the right operand is not_ const.");

	vx->RemoveLeadZero();
	vy->RemoveLeadZero();
	const std::size_t x_size = vx->GetSize();
	const std::size_t y_size = vy->GetSize();

	if(x_size == 0)
		throw std::invalid_argument("SemanticLib::great_const: size of the left operand is zero.");
	if(y_size == 0)
		throw std::invalid_argument("SemanticLib::great_const: size of the right operand is zero.");

	if(x_size > y_size)
	{
		vy->Resize(x_size);
	}
	else if(x_size < y_size)
	{
		// неравенство уже не выполняется
		return EncodeFactory::False();
	}

	const BitVector& x = *vx;
	const BitVector& y = *vy;
	LogicFormulaPtr equation = EncodeFactory::False();
	LogicFormulaPtr conj = EncodeFactory::True();
	for(int i = x_size - 1; i >= 0; --i)
	{
		const LogicFormulaPtr bit = y[i]->GetValue();
		if(bit->GetValue() == Transalg::LogicValueTrue)
		{
			conj = EncodeFactory::And(conj, x[i]->GetValue());
			conj = encode.CreateVarIfFunction(conj);
		}
		else
		{
			equation = EncodeFactory::Or(equation, EncodeFactory::And(conj, x[i]->GetValue()));
			equation = encode.CreateVarIfFunction(equation);
		}
	}
	return equation;
}

LogicFormulaPtr ge(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	LogicFormulaPtr g = great(x, y, encode);
	LogicFormulaPtr e = eq(x, y, encode);
	return EncodeFactory::Or(g, e);
}

LogicFormulaPtr ge_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	LogicFormulaPtr g = great_const(x, y, encode);
	LogicFormulaPtr e = eq_const(x, y, encode);
	return EncodeFactory::Or(g, e);
}

LogicFormulaPtr less(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	return great(y, x, encode);
}

LogicFormulaPtr less_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	// x, y менять местами нельзя, поэтому сначала >=, а потом отрицание
	return EncodeFactory::Not(ge_const(x, y, encode));
}

LogicFormulaPtr le(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	LogicFormulaPtr l = less(x, y, encode);
	LogicFormulaPtr e = eq(x, y, encode);
	return EncodeFactory::Or(l, e);
}

LogicFormulaPtr le_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	return EncodeFactory::Not(great_const(x, y, encode));
}

LogicFormulaPtr eq(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::eq: argument - null pointer.");

	LogicFormulaPtr equation;
	const BitVector& x = *vx;
	const BitVector& y = *vy;
	const std::size_t x_size = x.GetSize();
	const std::size_t y_size = y.GetSize();
	const std::size_t start = std::min(x_size, y_size);

	if(start == 0)
		throw std::invalid_argument("SemanticLib::eq: size of an operand is zero.");

	for(unsigned i = start; i > 0; --i)
	{
		equation = EncodeFactory::And(equation, EncodeFactory::Eq(x[i-1]->GetValue(), y[i-1]->GetValue()));
		equation = encode.CreateVarIfFunction(equation);
	}
	// старшие биты должны принимать значение 0
	if(x_size > y_size)
	{
		for(unsigned i = y_size; i < x_size; ++i)
		{
			equation = EncodeFactory::And(equation, EncodeFactory::Not(x[i]->GetValue()));
			equation = encode.CreateVarIfFunction(equation);
		}
	}
	else if(x_size < y_size)
	{
		for(unsigned i = x_size; i < y_size; i++)
		{
			equation = EncodeFactory::And(equation, EncodeFactory::Not(y[i]->GetValue()));
			equation = encode.CreateVarIfFunction(equation);
		}
	}
	return equation;
}

LogicFormulaPtr eq_const(BitVectorPtr vx, BitVectorPtr vy, EncodeFactory& encode)
{
	if(!vx || !vy)
		throw std::invalid_argument("SemanticLib::eq_const: argument - null pointer.");
	if(!vy->IsConst())
		throw std::invalid_argument("SemanticLib::eq_const: the right operand is not_ const.");

	vx->RemoveLeadZero();
	vy->RemoveLeadZero();
	const std::size_t x_size = vx->GetSize();
	const std::size_t y_size = vy->GetSize();

	if(x_size == 0)
		throw std::invalid_argument("SemanticLib::eq_const: size of the left operand is zero.");
	if(y_size == 0)
		throw std::invalid_argument("SemanticLib::eq_const: size of the right operand is zero.");

	if(x_size > y_size)
	{
		vy->Resize(x_size);
	}
	else if(x_size < y_size)
	{
		// неравенство уже не выполняется
		return EncodeFactory::False();
	}

	//! Размерности векторов vx и vy совпадают
	assert(vx->GetSize() == vy->GetSize());
	const BitVector& x = *vx;
	const BitVector& y = *vy;
	LogicFormulaPtr equation;
	for(unsigned i = 0; i < x_size; ++i)
	{
		const LogicFormulaPtr bit = y[i]->GetValue();
		if(bit->GetValue() == Transalg::LogicValueTrue)
			equation = EncodeFactory::And(equation, x[i]->GetValue());
		else
			equation = EncodeFactory::And(equation, EncodeFactory::Not(x[i]->GetValue()));
		equation = encode.CreateVarIfFunction(equation);
	}
	return equation;
}

LogicFormulaPtr neq(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	return EncodeFactory::Not(eq(x, y, encode));
}

LogicFormulaPtr neq_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode)
{
	return EncodeFactory::Not(eq_const(x, y, encode));
}

} // namespace SemanticLib
} // namespace Transalg
