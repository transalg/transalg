#include "stdafx.h"
#include "DataObject.h"
#include "DataTypeBit.h"
#include "DataTypeInteger.h"

namespace Transalg
{

bool DataObjectContext::AddObject(DataObjectPtr object)
{
	//! Проверим, что на текущем уровне нет объектов с таким же идентификатором
	for(DataObjectContainer::const_iterator it = objects_.begin();
		it != objects_.end();
		++it)
	{
		const DataObjectPtr& obj = *it;
		//! Всегда возвращаем первый попавшийся - предполагается, что на одном уровне нет одноименных объектов
		if(obj->GetObjectId() == object->GetObjectId())
		{
			return false;
		}
	}
	//! Добавляем в контейнер новый объект
	objects_.push_back(object);
	return true;
}

bool DataObjectContext::RemoveObject(const std::string& id)
{
	for(DataObjectContainer::iterator it = objects_.begin();
		it != objects_.end();
		++it)
	{
		const DataObjectPtr& object = *it;
		//! Всегда возвращаем первый попавшийся - предполагается, что на одном уровне нет одноименных объектов
		if(object->GetObjectId() == id)
		{
			objects_.erase(it);
			return true;
		}
	}
	return false;
}

DataObjectPtr DataObjectContext::FindObject(const std::string& id) const
{
	for(DataObjectContainer::const_iterator it = objects_.begin();
		it != objects_.end();
		++it)
	{
		const DataObjectPtr& object = *it;
		//! Всегда возвращаем первый попавшийся - предполагается, что на одном уровне нет одноименных объектов
		if(object->GetObjectId() == id)
		{
			return object;
		}
	}
	//! Если не нашли подходящий объект в текущем контексте, то переходим на уровень выше
	return (parent_ ? parent_->FindObject(id) : DataObjectPtr());
}

const DataObjectContext* DataObjectContext::GetParentContext() const
{
	return parent_;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// DataObjectVar

DataObjectVar::DataObjectVar(Attribute attribute, DataType data_type, const std::string& id, VoidPtr value)
	: DataObject(ObjectTypeVar, attribute, data_type, id)
{
	if(data_type == Transalg::dtInt)
	{
		const int init_value = (value ? toInt(value)->GetValue() : 0);
		data = boost::make_shared<Integer>(init_value);
	}
	else if(data_type == Transalg::dtBit)
	{
		BitPtr bit = value ? toBit(value) : BitPtr();
		data = bit ? boost::make_shared<Bit>(*bit) : boost::make_shared<Bit>();
	}
	else
		throw std::runtime_error("DataObjectVar (constructor): unknown data type.");
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// DataObjectArray

DataObjectArray::DataObjectArray(Attribute attribute, DataType data_type, 
	const std::string& id, const std::vector<unsigned>& indexes)
	: DataObject(ObjectTypeArray, attribute, data_type, id)
	, indexes_(indexes)
{
	if(indexes_.empty())
		throw std::invalid_argument("DataObjectArray (constructor): empty array.");

	unsigned size(1);
	unsigned i(0);
	for(; i < indexes.size(); ++i)
	{
		size *= indexes[i];
	}

	if(data_type == Transalg::dtInt)
	{
		for(i = 0; i < size; ++i)
			data_.push_back(boost::make_shared<Integer>());
	}
	else if(data_type == Transalg::dtBit)
	{
		for(i = 0; i < size; ++i)
			data_.push_back(boost::make_shared<Bit>());
	}
	else
		throw std::runtime_error("DataObjectArray (constructor): unknown data type.");
}

std::size_t DataObjectArray::GetSize() const
{
	return data_.size();
}

std::size_t DataObjectArray::GetIndexCount() const
{
	return indexes_.size();
}

unsigned DataObjectArray::GetIndexValue(unsigned index) const
{
	assert(index < indexes_.size());
	return (index < indexes_.size() ? indexes_[index] : 0);
}

const VoidPtr& DataObjectArray::operator[](unsigned index) const
{
	if(index >= data_.size())
		throw std::runtime_error("DataObjectArray::operator[]: out of array range.");
	return data_[index];
}

VoidPtr& DataObjectArray::operator[](unsigned index)
{
	if(index >= data_.size())
		throw std::runtime_error("DataObjectArray::operator[]: out of array range.");
	return data_[index];
}

VoidPtr DataObjectArray::GetValue(const std::vector<unsigned>& indexes) const
{
	if(indexes.empty())
		throw std::runtime_error("DataObjectArray::GetValue: index of the element is not defined.");
	
	//! Обращаемся к единственному элементу массива
	if(indexes.size() == indexes_.size())
	{
		const unsigned index = GetInnerIndex(indexes);
		return data_.at(index);
	}

	//! Обращение к подмассиву используется только для массивов типа Bit
	if(data_type_ != Transalg::dtBit)
		throw std::runtime_error("DataObjectArray::GetValue: specify not all indexes.");

	//! Группируем элементы массива в вектор
	BitContainer bits;
	const Range range = GetDataRange(indexes);
	for(VoidContainer::const_iterator it = range.first; it != range.second; ++it)
	{
		if(BitPtr bit = boost::dynamic_pointer_cast<Bit>(*it))
		{
			bits.push_back(bit);
		}
		else
			throw std::runtime_error("DataObjectArray::GetValue: unexpected pointer type.");
	}
	return boost::make_shared<BitVector>(bits);
}

BitVectorPtr DataObjectArray::GetValue() const
{
	//! Обращение к подмассиву используется только для массивов типа Bit
	if(data_type_ != Transalg::dtBit)
		throw std::runtime_error("DataObjectArray::GetValue: array indexes must be spedified.");

	BitContainer bits;
	for(VoidContainer::const_iterator it = data_.begin(); it != data_.end(); ++it)
	{
		if(BitPtr bit = boost::dynamic_pointer_cast<Bit>(*it))
		{
			bits.push_back(bit);
		}
		else
			throw std::runtime_error("DataObjectArray::GetValue: unexpected pointer type.");
	}
	return boost::make_shared<BitVector>(bits);
}

unsigned DataObjectArray::GetInnerIndex(const std::vector<unsigned>& indexes) const
{
	unsigned index = 0;
	for(unsigned i = 0; i < indexes.size(); ++i)
	{
		if(indexes[i] >= indexes_[i])
			throw std::runtime_error("DataObjectArray::GetInnerIndex: out of array range.");
		unsigned offset = indexes[i];
		for(unsigned j = i + 1; j < indexes_.size(); ++j)
		{
			offset *= indexes_[j];
		}
		index += offset;
	}
	return index;
}

DataObjectArray::Range DataObjectArray::GetDataRange(const std::vector<unsigned>& indexes) const
{
	if(indexes.size() > indexes_.size())
	{
		throw std::runtime_error("DataObjectArray::GetDataRange: out of array range.");
	}
	VoidContainer::const_iterator begin_it = data_.begin();
	//! Устанавливаем начальный итератор
	for(unsigned i = 0; i < indexes.size(); ++i)
	{
		unsigned index = indexes[i];
		//! index принадлежит отрезку [0, indexes_[i] - 1]
		if(index >= indexes_[i])
			throw std::runtime_error("DataObjectArray::GetDataRange: out of array range.");
		
		for(unsigned j = i + 1; j < indexes_.size(); ++j)
		{
			index *= indexes_[j];
		}
		std::advance(begin_it, index);
	}
	//! Устанавливаем конечный итератор
	VoidContainer::const_iterator end_it = begin_it;
	unsigned index = 1;
	for(unsigned i = indexes.size(); i < indexes_.size(); ++i)
	{
		index *= indexes_[i];
	}
	std::advance(end_it, index);
	return Range(begin_it, end_it);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// DataObjectFunction

DataObjectFunction::DataObjectFunction(const StatementFunctionDefinitionPtr& func_ptr)
	: DataObject(ObjectTypeFunction, Transalg::AttributeNone, func_ptr->GetDataType(), func_ptr->GetId())
	, func_ptr_(func_ptr)
{
	assert(func_ptr_);
}

std::size_t DataObjectFunction::GetArgCount() const
{
	return func_ptr_->GetArgsCount();
}

StatementBlockPtr DataObjectFunction::GetBody() const
{
	return func_ptr_->GetBody();
}

DataType DataObjectFunction::GetArgDataType(unsigned arg_num) const
{
	return func_ptr_->GetArgDataType(arg_num);
}

const std::string& DataObjectFunction::GetArgId(unsigned arg_num) const
{
	return func_ptr_->GetArgId(arg_num);
}

const std::vector<unsigned>& DataObjectFunction::GetArgIndexes(unsigned arg_num) const
{
	return func_ptr_->GetArgIndexes(arg_num);
}

} // namespace Transalg
