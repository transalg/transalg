#pragma once

#include <TransalgParser/Expression.h>
#include "DataTypes.h"
#include "DataObject.h"

namespace Transalg
{

class IALU
{
public:

	virtual ~IALU() {}

	//! Вычислить выражение
	virtual VoidPtr ExecuteExpression(ExpressionPtr expression, const DataObjectContext* context) = 0;

};

typedef boost::shared_ptr<IALU> IALUPtr;

} // namespace Transalg
