#pragma once

#include "DataTypes.h"
#include <TransalgParser/Types.h>
#include <TransalgParser/StatementBlock.h>
#include <TransalgParser/StatementFunctionDefinition.h>

namespace Transalg
{

//! Тип объекта ТА-программы
enum ObjectType
{
	ObjectTypeVar,
	ObjectTypeArray,
	ObjectTypeFunction
};

class DataObject
{ 
public:

	DataObject(ObjectType type, Attribute attribute, DataType data_type, const std::string& id)
		: type_(type)
		, data_type_(data_type)
		, attribute_(attribute)
		, id_(id)
	{}

	virtual ~DataObject() {}

	ObjectType GetObjectType() const {return type_;}

	DataType GetDataType() const {return data_type_;}

	Attribute GetAttribute() const {return attribute_;}

	const std::string& GetObjectId() const {return id_;}

protected:

	const ObjectType type_;
	const DataType data_type_;
	const Attribute attribute_;
	const std::string id_;
};

typedef boost::shared_ptr<DataObject> DataObjectPtr;
typedef std::vector<DataObjectPtr> DataObjectContainer;

class DataObjectContext
{
public:

	explicit DataObjectContext(const DataObjectContext* parent = 0): parent_(parent) {}

	const DataObjectContext* GetParentContext() const;

	bool AddObject(DataObjectPtr object);

	bool RemoveObject(const std::string& id);

	DataObjectPtr FindObject(const std::string& id) const;

private:

	DataObjectContainer objects_;
	
	const DataObjectContext* const parent_;
};

typedef boost::shared_ptr<DataObjectContext> DataObjectContextPtr;

///////////////////////////////////////////////////////////////////////////////////////////////////
//! Переменная ТА-программы
class DataObjectVar: public DataObject
{
public:

	DataObjectVar(Attribute attribute, DataType data_type, const std::string& id, VoidPtr value = VoidPtr());

	VoidPtr data;
};

typedef boost::shared_ptr<DataObjectVar> DataObjectVarPtr;
typedef std::vector<DataObjectVarPtr> DataObjectVarContainer;

//! Массив переменных ТА-программы
class DataObjectArray: public DataObject
{
public:

	DataObjectArray(Attribute attribute, DataType data_type, 
		const std::string& id, const std::vector<unsigned>& idx);

	const VoidPtr& operator[](unsigned index) const;
	VoidPtr& operator[](unsigned index);

	std::size_t GetSize() const;

	std::size_t GetIndexCount() const;

	unsigned GetIndexValue(unsigned index) const;

	//! indexes задает срез массива или единственный элемент
	//! извлекать срезы можно только для массивов типа Bit
	VoidPtr GetValue(const std::vector<unsigned>& indexes) const;

	//! Отдаем все элементы, сгруппированные в виде битового вектора
	BitVectorPtr GetValue() const;

private:

	typedef std::pair<VoidContainer::const_iterator, VoidContainer::const_iterator> Range;

	Range GetDataRange(const std::vector<unsigned>& indexes) const;

	unsigned GetInnerIndex(const std::vector<unsigned>& indexes) const;

private:

	VoidContainer data_;
	const std::vector<unsigned> indexes_;
};

typedef boost::shared_ptr<DataObjectArray> DataObjectArrayPtr;
typedef std::vector<DataObjectArrayPtr> DataObjectArrayContainer;

class DataObjectFunction: public DataObject
{
public:

	DataObjectFunction(const StatementFunctionDefinitionPtr& func_ptr);

	std::size_t GetArgCount() const;

	DataType GetArgDataType(unsigned arg_num) const;

	const std::string& GetArgId(unsigned arg_num) const;

	const std::vector<unsigned>& GetArgIndexes(unsigned arg_num) const;

	StatementBlockPtr GetBody() const;

private:

	//! Указатель на определение функции
	const StatementFunctionDefinitionPtr func_ptr_;
};

typedef boost::shared_ptr<DataObjectFunction> DataObjectFunctionPtr;
typedef std::vector<DataObjectFunctionPtr> DataObjectFunctionContainer;


} // namespace Transalg
