#pragma once

#include "DataTypeVoid.h"

namespace Transalg
{

//! Тип данных int, представляет 32-битное целое число
class Integer: public Void
{
public:

	Integer(int value = 0);

	// оператор присваивания
	Integer& operator=(const Integer&);
	Integer& operator=(const Bit&);
	Integer& operator=(const BitVector&);

	std::size_t GetSize() const {return 1;}

	bool IsConst() const {return true;}

	bool IsArray() const {return false;}

	std::string ToString() const;

	int GetValue()	const {return value_;}

	void SetValue(int value) {value_ = value;}

private:

	int value_;
};

} // namespace Transalg
