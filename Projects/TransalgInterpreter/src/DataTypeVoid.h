#pragma once

#include "DataTypes.h"
#include <TransalgParser/Types.h>

namespace Transalg
{

//! Тип данных void
class Void
{

public:

	Void(DataType type)
		: data_type_(type)
	{}

	virtual ~Void() {}

	DataType GetDataType() const {return data_type_;}

	bool IsInt() const {return data_type_ == Transalg::dtInt;}

	bool IsBit() const {return data_type_ == Transalg::dtBit;}

	virtual std::size_t GetSize() const = 0;

	virtual bool IsArray() const = 0;

	virtual bool IsConst() const = 0;

	virtual std::string ToString() const = 0;

protected:
	//! Тип данных
	DataType data_type_;
};

} // namespace Transalg
