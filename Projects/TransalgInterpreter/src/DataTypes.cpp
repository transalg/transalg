#include "stdafx.h"
#include "DataTypes.h"
#include "DataTypeInteger.h"
#include "DataTypeBit.h"
#include <TransalgFormats/EncodeFactory.h>

namespace Transalg
{

IntegerPtr toInt(VoidPtr x)
{
	if(!x)
		throw std::invalid_argument("Interpreter: toInt(): argument - null pointer.");
	if(!x->IsConst())
		throw std::invalid_argument("Interpreter: toInt(): argument must be a const.");

	if(IntegerPtr int_ptr = boost::dynamic_pointer_cast<Integer>(x))
	{
		assert(x->IsInt());
		return int_ptr;
	}
	else if(BitPtr bit_ptr = boost::dynamic_pointer_cast<Bit>(x))
	{
		assert(x->IsBit());
		const LogicFormulaPtr& formula = bit_ptr->GetValue();
		const int value = (formula->GetValue() == Transalg::LogicValueTrue ? 1 : 0);
		return boost::make_shared<Integer>(value);
	}
	else if(BitVectorPtr bit_vector_ptr = boost::dynamic_pointer_cast<BitVector>(x))
	{
		assert(x->IsBit() && x->IsArray());
		int value = 0;
		const std::size_t vec_size = bit_vector_ptr->GetSize();
		const std::size_t size = std::min(vec_size, sizeof(int));
		for(unsigned i = 0; i < size; ++i)
		{
			const LogicFormulaPtr& formula = bit_vector_ptr->GetBit(i)->GetValue();
			if(formula->GetValue() == Transalg::LogicValueTrue)
				value |= (1 << i);
		}
		return boost::make_shared<Integer>(value);
	}
	else
		throw std::runtime_error("toInt(): bad dynamic pointer cast.");
	return IntegerPtr();
}

BitPtr toBit(VoidPtr x)
{
	if(!x)
		throw std::invalid_argument("toBit(): argument - null pointer.");

	if(IntegerPtr int_ptr = boost::dynamic_pointer_cast<Integer>(x))
	{
		assert(x->IsInt());
		LogicFormulaPtr value = (int_ptr->GetValue() == 0 ? EncodeFactory::False() : EncodeFactory::True());
		return boost::make_shared<Bit>(value);
	}
	else if(BitPtr bit_ptr = boost::dynamic_pointer_cast<Bit>(x))
	{
		assert(x->IsBit());
		return bit_ptr;
	}
	else if(BitVectorPtr bit_vector_ptr = boost::dynamic_pointer_cast<BitVector>(x))
	{
		assert(x->IsBit() && x->IsArray());
		LogicFormulaPtr value = bit_vector_ptr->GetBit(0)->GetValue();
		return boost::make_shared<Bit>(value);
	}
	else
		throw std::runtime_error("toBit(): bad dynamic pointer cast.");
	return BitPtr();
}

unsigned MajorBitPos(unsigned x)
{
	unsigned pos = 0;
	while(x)
	{
		x >>= 1;
		++pos;
	}
	return pos;
}

BitVectorPtr toBitVector(VoidPtr x)
{
	if(!x)
		throw std::invalid_argument("toBitVector(): argument - null pointer.");

	if(IntegerPtr int_ptr = boost::dynamic_pointer_cast<Integer>(x))
	{
		assert(x->IsInt());
		const int value = int_ptr->GetValue();
		size_t pos = MajorBitPos(static_cast<unsigned>(value));

		LogicFormulaContainer data;
		for(unsigned i = 0; i < pos; ++i)
		{
			data.push_back( (static_cast<unsigned>(value) & (1 << i)) > 0 ? EncodeFactory::True() : EncodeFactory::False() );
		}

		//! Случай, если value == 0. Вектор не может быть пустым, поэтому добавляем нулевой бит.
		if(data.size() == 0)
			data.push_back(EncodeFactory::False());

		return boost::make_shared<BitVector>(data);
	}
	else if(BitPtr bit_ptr = boost::dynamic_pointer_cast<Bit>(x))
	{
		assert(x->IsBit());
		LogicFormulaContainer data;
		data.push_back(bit_ptr->GetValue());
		return boost::make_shared<BitVector>(data);
	}
	else if(BitVectorPtr bit_vector_ptr = boost::dynamic_pointer_cast<BitVector>(x))
	{
		assert(x->IsBit() && x->IsArray());
		return bit_vector_ptr;
	}
	else
		throw std::runtime_error("toBit(): bad dynamic pointer cast.");
	return BitVectorPtr();
}

void PrintData(std::ostream& out, VoidPtr x)
{
	if(!x)
		throw std::invalid_argument("PrintData(): argument - null pointer.");

	out << x->ToString() << std::endl;
}

} // namespace Transalg
