#pragma once

#include <vector>
#include <set>
#include <map>
#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

namespace Transalg
{

//! Типы данных ТА-программы
enum DataType
{
	dtVoid, 
	dtInt, 
	dtBit
};

//! Атрибуты
enum Attribute
{
	AttributeNone,
	AttributeIn,
	AttributeOut,
	AttributeMem
};


// forward declaration
class Statement;
class StatementBlock;
class StatementVarDefinition;
class StatementArrayDefinition;
class StatementFunctionDefinition;
class StatementFunctionCall;
class StatementAssign;
class StatementIf;
class StatementFor;
class StatementReturn;
class StatementTable;

typedef unsigned int uint;

typedef boost::shared_ptr<StatementBlock> StatementBlockPtr;

} // namespace Transalg
