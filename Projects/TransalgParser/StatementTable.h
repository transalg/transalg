#pragma once

#include "Types.h"
#include "Statement.h"

namespace Transalg
{

/*
//! Cинтаксис
table SBox[3 3 2 : 3]
{
	[1 2 1 : 5]
	[3 1 2 : 7]
	[6 3 2 : 3]
	[2 1 1 : 1]
}
*/

//! Табличное представление произвольной дискретной функции
//! Входные параметры и возвращаемый результат - это переменные или одномерные массивы типа bit
class StatementTable: public Statement
{
	friend class Parser;
public:

	struct TableRow
	{
		TableRow(std::size_t in_size = 1, std::size_t out_size = 1)
			: in_values(in_size, 0)
			, out_values(out_size, 0)
		{}
		std::vector<unsigned> in_values;
		std::vector<unsigned> out_values;
	};

	explicit StatementTable(const StatementBlock* context, unsigned line, const std::string& id)
		: Statement(context, stmtTable, line)
		, id_(id)
	{}

	const std::string& GetId() const;

	std::size_t GetInputSize() const;

	std::size_t GetOutputSize() const;

	std::size_t GetTableSize() const;

	const TableRow& GetRow(unsigned index) const;

	unsigned GetInputBitSize(unsigned index) const;

	unsigned GetOutputBitSize(unsigned index) const;

private:

	//! Идентификатор функции
	std::string id_;

	//! Размер в битах каждого входного параметра
	std::vector<unsigned> input_bit_size_;

	//! Размер в битах каждого выходного параметра
	std::vector<unsigned> output_bit_size_;

	typedef std::vector<TableRow> Table;
	Table table_;
};

typedef boost::shared_ptr<StatementTable> StatementTablePtr;

} // namespace Transalg
