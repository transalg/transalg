#pragma once

#include "Statement.h"
#include "StatementBlock.h"

namespace Transalg
{

class Parser
{
public:
	
	Parser();

	StatementBlockPtr ParseProgram(const std::string& filename);

	StatementBlockPtr ParseProgramText(const std::string& text);

private:

	class Impl;
	typedef boost::shared_ptr<Impl> ImplPtr;
	ImplPtr impl_;
};

} // namespace Transalg
