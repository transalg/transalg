#pragma once

namespace Transalg
{

class ParseError : public std::exception
{
public:

	explicit ParseError(const char* const message);
	explicit ParseError(const std::string& message);

public: 
	
	//! std::exception interface
	virtual const char * what() const throw();

	const std::string& what_str() const throw();

	virtual ~ParseError() throw() {}

private:

	const ParseError& operator=(const ParseError&);

private:

	const std::string msg_;
};


} // namespace Transalg
