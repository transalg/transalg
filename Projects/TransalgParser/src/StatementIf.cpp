#include "stdafx.h"
#include "../StatementIf.h"

namespace Transalg
{

ExpressionPtr StatementIf::GetCondition() const
{
	return condition_;
}

StatementPtr StatementIf::GetTrueBranch() const
{
	return true_branch_;
}

StatementPtr StatementIf::GetFalseBranch() const
{
	return false_branch_;
}

} // namespace Transalg
