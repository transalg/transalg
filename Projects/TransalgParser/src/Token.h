#pragma once

#include <string>
#include <vector>

namespace Transalg
{

//! Тип токена
enum TokenType
{
	ttEmpty,
	ttError,
	ttEof,
	ttAttribute,
	ttDataType,
	ttId,
	ttNumber,
	ttKeyWord,
	ttDelimiter,
	ttOperator
};

//! Код токена
enum TokenCode
{
	tcNone,
	tcDecNumber,
	tcBitNumber,
	tcHexNumber,
	tcFor,
	tcIf,
	tcElse,
	tcReturn,
	tcDefine,
	tcTable,
	tcIn,
	tcOut,
	tcMem,
	tcVoid,
	tcInt,
	tcBit,
	tcLeftBrace, 
	tcRightBrace, 
	tcLeftRoundBrace, 
	tcRightRoundBrace,
	tcLeftSquareBrace, 
	tcRightSquareBrace, 
	tcComma, 
	tcSemicolumn, 
	tcColon,
	tcAssign, 
	tcSum, 
	tcSub, 
	tcMul, 
	tcDiv, 
	tcMod, 
	tcAnd, 
	tcOr, 
	tcXor, 
	tcNot, 
	tcConcat, 
	tcLeftShift, 
	tcRightShift, 
	tcLeftSaveShift, 
	tcRightSaveShift, 
	tcLeftCircleShift, 
	tcRightCircleShift,
	tcGreat, 
	tcGreatEq, 
	tcLess, 
	tcLessEq, 
	tcEqual, 
	tcNotEqual
};

struct Token
{
	Token(TokenType token_type = ttEmpty, 
		TokenCode token_code = tcNone, 
		std::string token_text = std::string(), 
		unsigned token_line = 0, unsigned token_col = 0)
		: type(token_type)
		, code(token_code)
		, text(token_text)
		, line(token_line)
		, col (token_col)
	{}

	TokenType type;
	TokenCode code;
	std::string text;
	//! Строка программы
	unsigned line;
	//! Позиция в строке
	unsigned col;
};

typedef std::vector<Token> TokenStream;

} // namespace Transalg
