#include "../StatementArrayDefinition.h"

namespace Transalg
{

Attribute StatementArrayDefinition::GetAttribute() const
{
	return attribute_;
}

DataType StatementArrayDefinition::GetDataType() const
{
	return data_type_;
}

const std::string& StatementArrayDefinition::GetId() const
{
	return id_;
}

std::size_t StatementArrayDefinition::GetIndexCount() const
{
	return array_idxs_.size();
}

unsigned StatementArrayDefinition::GetIndex(unsigned index) const
{
	return array_idxs_.at(index);
}

std::size_t StatementArrayDefinition::GetInitValueCount() const
{
	return init_values_.size();
}

unsigned StatementArrayDefinition::GetInitValue(unsigned value_index) const
{
	return init_values_.at(value_index);
}

ExpressionPtr StatementArrayDefinition::GetInitExpression() const
{
	return init_expression_;
}

} // namespace Transalg
