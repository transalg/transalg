#include "../StatementVarDefinition.h"

namespace Transalg
{

Attribute StatementVarDefinition::GetAttribute() const
{
	return attribute_;
}

DataType StatementVarDefinition::GetDataType() const
{
	return data_type_;
}

const std::string& StatementVarDefinition::GetId() const
{
	return id_;
}

ExpressionPtr StatementVarDefinition::GetInitValue() const
{
	return init_value_;
}

} // namespace Transalg
