#include "stdafx.h"
#include "../Serializer.h"
#include "../StatementVarDefinition.h"
#include "../StatementArrayDefinition.h"
#include "../StatementFunctionDefinition.h"
#include "../StatementBlock.h"
#include "../StatementFor.h"
#include "../StatementIf.h"
#include "../StatementReturn.h"
#include "../StatementTable.h"
#include "../Expression.h"

namespace Transalg
{

namespace
{

std::string AttributeText(Attribute attr)
{
	switch(attr)
	{
	case Transalg::AttributeIn:
		return std::string("__in");
	case Transalg::AttributeOut:
		return std::string("__out");
	case Transalg::AttributeMem:
		return std::string("__mem");
	}
	return std::string();
}

std::string DatatypeText(DataType data_type)
{
	switch(data_type)
	{
	case Transalg::dtVoid:
		return std::string("void");
	case Transalg::dtInt:
		return std::string("int");
	case Transalg::dtBit:
		return std::string("bit");
	}
	return std::string();
}

void SerializeArrayInit(std::ostream& out, 
						const StatementArrayDefinition& stmt,
						unsigned index_num, unsigned& offset)
{
	const unsigned index_count = stmt.GetIndexCount();
	assert(index_num < index_count);
	out << "{";
	const unsigned size = stmt.GetIndex(index_num);
	if(index_num + 1 == index_count)
	{
		for(unsigned i = 0; i < size; ++i)
			out << (i==0?"":",") << stmt.GetInitValue(offset + i);
		offset += size;
	}
	else
	{
		for(unsigned i = 0; i < size; ++i)
			SerializeArrayInit(out, stmt, index_num + 1, offset);
	}
	out << "}";
}

} // local namespace

void Serializer::SerializeStatement(std::ostream& out, const StatementPtr& stmt)
{
	if(!stmt) return;
	switch(stmt->GetStatementType())
	{
	case stmtExpression:
		{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(stmt);
		SerializeExpression(out, *ptr);
		out << std::endl;
		}
		break;
	case stmtVarDefinition:
		{
		StatementVarDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementVarDefinition>(stmt);
		SerializeStatementVarDefinition(out, *ptr);
		}
		break;
	case stmtArrayDefinition:
		{
		StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(stmt);
		SerializeStatementArrayDefinition(out, *ptr);
		}
		break;
	case stmtFunctionDefinition:
		{
		StatementFunctionDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementFunctionDefinition>(stmt);
		SerializeStatementFunctionDefinition(out, *ptr);
		}
		break;
	case stmtBlock:
		{
		StatementBlockPtr ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		SerializeStatementBlock(out, *ptr);
		}
		break;
	case stmtFor:
		{
		StatementForPtr ptr = boost::dynamic_pointer_cast<StatementFor>(stmt);
		SerializeStatementFor(out, *ptr);
		}
		break;
	case stmtIf:
		{
		StatementIfPtr ptr = boost::dynamic_pointer_cast<StatementIf>(stmt);
		SerializeStatementIf(out, *ptr);
		}
		break;
	case stmtReturn:
		{
		StatementReturnPtr ptr = boost::dynamic_pointer_cast<StatementReturn>(stmt);
		SerializeStatementReturn(out, *ptr);
		}
		break;
	case stmtTable:
		{
		StatementTablePtr ptr = boost::dynamic_pointer_cast<StatementTable>(stmt);
		SerializeStatementTable(out, *ptr);
		}
		break;
	default:
		throw std::runtime_error("Serializer: Unknown statement type!");
	}
}

void Serializer::SerializeExpression(std::ostream& out, const Expression& expression)
{
	const unsigned op_count = expression.GetOperandsCount();
	for(unsigned i = 0; i < op_count; ++i)
	{
		const ExpressionOperand& operand = expression.GetOperand(i);
		//! Пока выводим только текст
		out << (i==0?"":" ") << operand.id_;
	}
	out << " (size = " << op_count << ")";
}

void Serializer::SerializeStatementVarDefinition(std::ostream& out, const StatementVarDefinition& stmt)
{
	out << "[var definition]: " << AttributeText(stmt.GetAttribute())
		<< " " << DatatypeText(stmt.GetDataType()) 
		<< " " << stmt.GetId();
	ExpressionPtr init_value = stmt.GetInitValue();
	if(init_value.get())
	{
		out << " = ";
		SerializeExpression(out, *init_value);
	}
	out << ";" << std::endl;
}

void Serializer::SerializeStatementArrayDefinition(std::ostream& out, const StatementArrayDefinition& stmt)
{
	out << "[array definition]: " << AttributeText(stmt.GetAttribute()) 
		<< " " << DatatypeText(stmt.GetDataType())
		<< " " << stmt.GetId();
	//! Объявленные размеры
	const unsigned index_count = stmt.GetIndexCount();
	for(unsigned i = 0; i < index_count; ++i)
	{
		out << "[" << stmt.GetIndex(i) << "]";
	}
	if(stmt.GetInitValueCount())
	{
		out << " = ";
		unsigned offset(0);
		SerializeArrayInit(out, stmt, 0, offset);
		assert(offset == stmt.GetInitValueCount());
	}
	else if(ExpressionPtr init_expression = stmt.GetInitExpression())
	{
		out << " = ";
		SerializeExpression(out, *init_expression);
	}
	out << ";" << std::endl;
}

void Serializer::SerializeStatementFunctionDefinition(std::ostream& out, const StatementFunctionDefinition& stmt)
{
	out << "[function definition]: " << DatatypeText(stmt.GetDataType())
		<< " " << stmt.GetId();
	out << "(";
	for(unsigned i = 0, arg_count = stmt.GetArgsCount(); i < arg_count; ++i)
	{
		out << (i==0?"":", ") << DatatypeText(stmt.GetArgDataType(i)) << " " << stmt.GetArgId(i);
		const std::vector<unsigned>& arg_idxs = stmt.GetArgIndexes(i);
		for(unsigned j = 0; j < arg_idxs.size(); ++j)
			out << "[" << arg_idxs[j] << "]";
	}
	out << ")" << std::endl;
	//! Тело функции - блок
	StatementBlockPtr body = stmt.GetBody();
	SerializeStatementBlock(out, *body);
	out << std::endl;
}

void Serializer::SerializeStatementBlock(std::ostream& out, const StatementBlock& stmt)
{
	out << (stmt.GetFunction() ? "[begin function body]" : "[begin block]") << std::endl << std::endl;
	for(StatementContainer::const_iterator it = stmt.begin();
		it != stmt.end();
		++it)
	{
		SerializeStatement(out, *it);
		out << std::endl;
	}
	out << (stmt.GetFunction() ? "[end function body]" : "[end block]") << std::endl;
}

void Serializer::SerializeStatementFor(std::ostream& out, const StatementFor& stmt)
{
	out << "[begin for statement]" << std::endl;
	StatementPtr init_stmt = stmt.GetInitStatement();
	out << "[for statement: init] ";
	if(init_stmt) SerializeStatement(out, init_stmt);
	out << std::endl;
	out << "[for statement: cond] ";
	ExpressionPtr expr_ptr = stmt.GetConditionExpression();
	if(expr_ptr) SerializeExpression(out, *expr_ptr);
	out << std::endl;
	out << "[for statement: step] ";
	expr_ptr = stmt.GetStepExpression();
	if(expr_ptr) SerializeExpression(out, *expr_ptr);
	out << std::endl;
	SerializeStatement(out, stmt.GetBody());
	out << "[end for statement]" << std::endl;
}

void Serializer::SerializeStatementIf(std::ostream& out, const StatementIf& stmt)
{
	out << "[begin if statement]" << std::endl;
	out << "[if statement: cond] ";
	ExpressionPtr expr_ptr = stmt.GetCondition();
	if(expr_ptr) SerializeExpression(out, *expr_ptr);
	//! true-branch
	out << std::endl << "[if statement: true-branch]:" << std::endl << std::endl;
	SerializeStatement(out, stmt.GetTrueBranch());
	//! false-branch
	out << std::endl << "[if statement: false-branch]:" << std::endl << std::endl;
	SerializeStatement(out, stmt.GetFalseBranch());
	out << "[end if statement]" << std::endl;
}

void Serializer::SerializeStatementReturn(std::ostream& out, const StatementReturn& stmt)
{
	out << "[statement return] ";
	if(ExpressionPtr expr_ptr = stmt.GetReturnExpression())
	{
		SerializeExpression(out, *expr_ptr);
	}
	out << std::endl;
}

void Serializer::SerializeStatementTable(std::ostream& out, const StatementTable& stmt)
{
	unsigned i;
	out << "[begin table]" << std::endl;
	out << "id: " << stmt.GetId() 
		<< ", input size: " << stmt.GetInputSize() 
		<< ", output size: " << stmt.GetOutputSize() 
		<< ", row count: " << stmt.GetTableSize() << std::endl;
	//! Размер входных параметров (в битах)
	out << "[input bit size]:";
	for(i = 0; i < stmt.GetInputSize(); ++i)
	{
		out << " " << stmt.GetInputBitSize(i);
	}
	out << std::endl;
	//! Размер выходных параметров (в битах)
	out << "[output bit size]:";
	for(i = 0; i < stmt.GetOutputSize(); ++i)
	{
		out << " " << stmt.GetOutputBitSize(i);
	}
	out << std::endl;
	//! Содержание таблицы
	for(unsigned idx = 0; idx < stmt.GetTableSize(); ++idx)
	{
		const StatementTable::TableRow& row = stmt.GetRow(idx);
		out << "[";
		for(i = 0; i < row.in_values.size(); ++i)
		{
			out << row.in_values[i] << " ";
		}
		out << ":";
		for(i = 0; i < row.out_values.size(); ++i)
		{
			out << " " << row.out_values[i];
		}
		out << "]" << std::endl;
	}
	out << "[end table]" << std::endl;
}

} // namespace Transalg
