#include "stdafx.h"
#include "../StatementFunctionDefinition.h"

namespace Transalg
{

StatementFunctionDefinition::StatementFunctionDefinition(const StatementBlock* context, unsigned line, 
														 DataType data_type, const std::string id)
	: Statement(context, stmtFunctionDefinition, line)
	, data_type_(data_type)
	, id_(id)
{
	block_ = boost::make_shared<StatementBlock>(context_, stmtBlock, this);
}

DataType StatementFunctionDefinition::GetDataType() const
{
	return data_type_;
}

const std::string& StatementFunctionDefinition::GetId() const
{
	return id_;
}

std::size_t StatementFunctionDefinition::GetArgsCount() const
{
	assert(args_type_.size() == args_id_.size());
	return args_type_.size();
}

DataType StatementFunctionDefinition::GetArgDataType(unsigned arg_num) const
{
	return args_type_.at(arg_num);
}

const std::string& StatementFunctionDefinition::GetArgId(unsigned arg_num) const
{
	return args_id_.at(arg_num);
}

const std::vector<unsigned>& StatementFunctionDefinition::GetArgIndexes(unsigned arg_num) const
{
	return arg_indexes_.at(arg_num);
}

StatementBlockPtr StatementFunctionDefinition::GetBody() const
{
	return block_;
}

} // namespace Transalg
