#include "stdafx.h"
#include "../StatementReturn.h"

namespace Transalg
{

ExpressionPtr StatementReturn::GetReturnExpression() const
{
	return return_expression_;
}

} // namespace Transalg
