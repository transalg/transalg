#include "stdafx.h"
#include "../ParseError.h"

namespace Transalg
{

ParseError::ParseError(const char* const message)
	: msg_(message)
{}

ParseError::ParseError(const std::string& message)
	: msg_(message)
{}

const char* ParseError::what() const throw()
{
	return msg_.c_str();
}

const std::string& ParseError::what_str() const throw()
{
	return msg_;
}

} // namespace Transalg
