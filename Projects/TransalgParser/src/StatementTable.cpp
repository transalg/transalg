#include "stdafx.h"
#include "../StatementTable.h"

namespace Transalg
{

const std::string& StatementTable::GetId() const
{
	return id_;
}

std::size_t StatementTable::GetInputSize() const
{
	return input_bit_size_.size();
}

std::size_t StatementTable::GetOutputSize() const
{
	return output_bit_size_.size();
}

std::size_t StatementTable::GetTableSize() const
{
	return table_.size();
}

const StatementTable::TableRow& StatementTable::GetRow(unsigned index) const
{
	return table_.at(index);
}

unsigned StatementTable::GetInputBitSize(unsigned index) const
{
	return input_bit_size_.at(index);
}

unsigned StatementTable::GetOutputBitSize(unsigned index) const
{
	return output_bit_size_.at(index);
}

}// namespace Transalg
