#include "stdafx.h"
#include "../ParseError.h"
#include "../Parser.h"
#include "../StatementVarDefinition.h"
#include "../StatementArrayDefinition.h"
#include "../StatementFunctionDefinition.h"
#include "../StatementBlock.h"
#include "../StatementFor.h"
#include "../StatementIf.h"
#include "../StatementReturn.h"
#include "../StatementTable.h"
#include "Lexer.h"
#include <stack>

namespace Transalg
{

class Parser::Impl
{
public:

	Impl() {}

	StatementBlockPtr ParseProgram(const std::string& filename);

	StatementBlockPtr ParseProgramText(const std::string& text);

private:

	StatementBlockPtr ParseProgram();

	StatementPtr ParseStatement();
	StatementVarDefinitionPtr ParseVarDefinition(const Token& data_type, const Token& id, const Token attribute = Token());
	StatementArrayDefinitionPtr ParseArrayDefinition(const Token& data_type, const Token& id, const Token attribute = Token());
	StatementFunctionDefinitionPtr ParseFunctionDefinition(const Token& data_type, const Token& id);

	StatementBlockPtr ParseStatementBlock(StatementFunctionDefinition* func_ptr);
	StatementIfPtr ParseStatementIf();
	StatementForPtr ParseStatementFor();
	StatementTablePtr ParseTableFunction();

	//! Рекурсивная процедура разбора выражения-инициализации для многомерного массива
	//! level - текущий уровень вложенности
	void ParseArrayInitialize(std::vector<unsigned>& indexes, unsigned level, std::vector<unsigned>& init_values);

	ExpressionPtr ParseExpression();
	unsigned ParseFunctionCallArguments(std::vector<ExpressionOperand>& poliz);
	unsigned ParseArrayArguments(std::vector<ExpressionOperand>& poliz);

	//! Получить обозреваемый токен
	const Token& GetToken() const;
	//! Получить следующий токен
	const Token& GetNextToken();
	//! Получить следующий токен, убедившись, что это идентификатор
	const Token& GetNextTokenId();
	//! Получить следующий токен, убедившись, что это число
	const Token& GetNextTokenNumber();
	//! Получить следующий токен, убедившись, что это тип данных
	const Token& GetNextTokenDatatype();

	Token GetLookAheadToken() const;

	std::string TokenInfo(const Token& token) const;

	Attribute GetAttribute(Transalg::TokenCode code) const;
	DataType GetDataType(Transalg::TokenCode code) const;

	static bool IsGlobalContext(const StatementBlock* context)
	{
		return (context->GetContext() ? false : true);
	}


private:

	TokenStream tokens_;
	TokenStream::const_iterator token_cursor_;

	const StatementBlock* current_context_;

	typedef std::map<std::string, std::string> DefineConstMap;
	DefineConstMap define_map_;
};

std::string Parser::Impl::TokenInfo(const Token& token) const
{
	std::stringstream ss;
	ss << "'" << token.text << "'"
		<< ", type = " << token.type 
		<< ", code = " << token.code
		<< ", line = " << token.line
		<< ", column = " << token.col;
	return ss.str();
}

StatementBlockPtr Parser::Impl::ParseProgram(const std::string& filename)
{
	//! Прочитать текст программы из файла и построить последовательность токенов
	std::ifstream in(filename.c_str(), std::ios::in);
	if(in.is_open())
	{
		std::stringstream ss;
		ss << in.rdbuf();

		Lexer lexer;
		if(!lexer.ParseTokens(ss.str(), tokens_))
		{
			throw ParseError("Some error occure while parse tokens.");
		}
		in.close();
	}
	else
	{
		throw std::runtime_error(std::string("Can't open file with program: ") + filename);
	}
	return ParseProgram();
}

StatementBlockPtr Parser::Impl::ParseProgramText(const std::string& text)
{
	Lexer lexer;
	if(!lexer.ParseTokens(text, tokens_))
	{
		throw ParseError("Some error occure while parse tokens.");
	}
	return ParseProgram();
}

StatementBlockPtr Parser::Impl::ParseProgram()
{
	//! Глобальная область видимости
	StatementBlockPtr scope_ptr = 
		boost::make_shared<StatementBlock>(static_cast<const StatementBlock*>(0), 0, 
		static_cast<StatementFunctionDefinition*>(0));

	current_context_ = scope_ptr.get();

	//! Разбор инструкций программы
	for(token_cursor_ = tokens_.begin(); token_cursor_ != tokens_.end(); ++token_cursor_)
	{
		if(StatementPtr stmt = ParseStatement())
		{
			scope_ptr->statements_.push_back(stmt);
		}
	}
	return scope_ptr;
}

StatementPtr Parser::Impl::ParseStatement()
{
	StatementPtr stmt;
	
	const Token& token = GetToken();
	//! Атрибут - опередение переменной или массива типа bit
	if(token.type == Transalg::ttAttribute)
	{
		//! Атрибуты __in и __out используются только в глобальной области видимости
		if(!IsGlobalContext(current_context_) && 
			(token.code == Transalg::tcIn || token.code == Transalg::tcOut))
		{
			throw ParseError("ParseStatement: attributes may be used only in the global scope!");
		}

		//! Атрибут
		const Token& attribute(token);
		//! Тип данных
		const Token& data_type = GetNextTokenDatatype();
		//! Идентификатор
		const Token& data_id = GetNextTokenId();

		if(data_type.code != Transalg::tcBit)
		{
			throw ParseError("ParseStatement: Unexpected token type (" + 
				TokenInfo(data_type) + ") - expect token 'bit' (data type).");
		}

		//! Следующий токен: 
		const Token& delim = GetNextToken();
		//! ';' или '=' - для переменной
		if((delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn) || 
			(delim.type == Transalg::ttOperator && delim.code == Transalg::tcAssign))
		{
			stmt = ParseVarDefinition(data_type, data_id, attribute);
		}
		//! '[' - для массива
		else if(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcLeftSquareBrace)
		{
			stmt = ParseArrayDefinition(data_type, data_id, attribute);
		}
		else
		{
			throw ParseError("ParseStatement: Unexpected token type (" + 
				TokenInfo(delim) + ") - expect token ';', '=' or '['.");
		}
		return stmt;
	}
	
	//! Тип данных - определение переменной, массива или функции
	if(token.type == Transalg::ttDataType)
	{
		const Token& data_type(token);
		//! Читаем тип данных
		const Token& data_id = GetNextTokenId();
		//! Следующий токен: 
		const Token& delim = GetNextToken();
		//! ';' или '=' - для переменной
		if((delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn) || 
			(delim.type == Transalg::ttOperator && delim.code == Transalg::tcAssign))
		{
			stmt = ParseVarDefinition(data_type, data_id);
		}
		//! '[' - для массива
		else if(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcLeftSquareBrace)
		{
			stmt = ParseArrayDefinition(data_type, data_id);
		}
		//! '(' - для функции
		else if(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcLeftRoundBrace)
		{
			stmt = ParseFunctionDefinition(data_type, data_id);
		}
		else
		{
			throw ParseError("ParseStatement: Unexpected token type (" + 
				TokenInfo(delim) + ") - expect token ';', '=', '[' or '('.");
		}
		return stmt;
	}

	//! Инструкции, начинающиеся с ключевого слова - define, for, if, return
	if(token.type == Transalg::ttKeyWord)
	{
		if(token.code == Transalg::tcDefine)
		{
			//! Идентификатор целочисленной константы
			const Token& data_id = GetNextTokenId();
			//! Значение целочисленной константы
			const Token& data_value = GetNextTokenNumber();
			//! Разделитель
			const Token& delim = GetNextToken();
			if(!(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn))
			{
				throw ParseError("ParseStatement: Unexpected token type (" + 
					TokenInfo(delim) + ") - expect token ';'.");
			}
			define_map_.insert(std::make_pair(data_id.text, data_value.text));
			return StatementPtr();
		}
		else if(token.code == Transalg::tcFor)
		{
			stmt = ParseStatementFor();
		}
		else if(token.code == Transalg::tcIf)
		{
			stmt = ParseStatementIf();
		}
		else if(token.code == Transalg::tcReturn)
		{
			GetNextToken();
			StatementReturnPtr stmt_return = boost::make_shared<StatementReturn>(current_context_, token.line);
			stmt_return->return_expression_ = ParseExpression();
			Token delim = GetToken();
			if(!(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn)) // ';'
			{
				throw ParseError("ParseStatement: Unexpected token type (" + 
					TokenInfo(delim) + ") - expect token ';'.");
			}
			stmt = stmt_return;
		}
		else if(token.code == Transalg::tcTable)
		{
			stmt = ParseTableFunction();
		}
		//! Неизвестное ключевое слово - exception
		else
		{
			throw ParseError("ParseStatement: unknown keyword (" + TokenInfo(token) + ")");
		}
		return stmt;
	}

	//! Выражения могут начинаться с идентификатора, константы, открывающейся круглой скобки, унарного оператора (-, !)
	if(token.type == Transalg::ttId 
		|| token.type == Transalg::ttNumber || 
		(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace) ||
		(token.type == Transalg::ttOperator && (token.code == Transalg::tcNot || token.code == Transalg::tcSub))
		)
	{
		stmt = ParseExpression();
		const Token& delim = GetToken();
		if(!(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn))
		{
			throw ParseError("ParseStatement: Unexpected token type (" + TokenInfo(delim) + ") - expect token ';'.");
		}
		return stmt;
	}

	//! Составной оператор (блок)
	if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace) // '{'
	{
		return ParseStatementBlock(static_cast<StatementFunctionDefinition*>(0));
	}

	return StatementPtr();
}

StatementVarDefinitionPtr Parser::Impl::ParseVarDefinition(const Token& data_type, const Token& id, const Token attribute)
{
	//! Строка, на которой начинается определение переменной
	const unsigned line = (attribute.type == Transalg::ttAttribute ? attribute.line : data_type.line);
	const Attribute attr = GetAttribute(attribute.code);
	const DataType type = GetDataType(data_type.code);

	StatementVarDefinitionPtr stmt_ptr = 
		boost::make_shared<StatementVarDefinition>(current_context_, line, attr, type, id.text);

	//! Разбор инициализирующего выражения, если оно есть
	const Token& token = GetToken();
	if(token.type == Transalg::ttOperator && token.code == Transalg::tcAssign) // '='
	{
		GetNextToken();
		stmt_ptr->init_value_ = ParseExpression();
	}

	//! Заключительным токеном должен быть ';'
	const Token& delim = GetToken();
	if(!(delim.type == Transalg::ttDelimiter && delim.code == Transalg::tcSemicolumn)) // ';'
	{
		throw ParseError("ParseVarDefinition: Unexpected token type (" + TokenInfo(delim) + ") - expect token ';'.");
	}

	return stmt_ptr;
}

StatementArrayDefinitionPtr Parser::Impl::ParseArrayDefinition(const Token& data_type, const Token& id, const Token attribute)
{
	//! Строка, на которой начинается определение переменной
	const unsigned line = (attribute.type == Transalg::ttAttribute ? attribute.line : data_type.line);
	const Attribute attr = GetAttribute(attribute.code);
	const DataType type = GetDataType(data_type.code);

	StatementArrayDefinitionPtr stmt_ptr = 
		boost::make_shared<StatementArrayDefinition>(current_context_, line, attr, type, id.text);

	//! Разбор списка индексов
	Token token = GetToken();
	while(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace) // '['
	{
		unsigned index(0);
		token = GetNextToken();
		//! Индексы массивов должны задаваться целыми числами в десятичной нотации
		if(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber)
		{
			index = boost::lexical_cast<unsigned>(token.text);
		}
		//! Либо именованной константой
		else if(token.type == Transalg::ttId)
		{
			DefineConstMap::const_iterator define_it = define_map_.find(token.text);
			if(define_it != define_map_.end())
			{
				index = boost::lexical_cast<unsigned>(define_it->second);
			}
			else
				throw ParseError("ParseArrayDefinition: index must be an integer (" + TokenInfo(token) + ").");
		}
		//! Размерность массива не может быть нулевой
		if(!index)
			throw ParseError("ParseArrayDefinition: index == 0.");
		
		stmt_ptr->array_idxs_.push_back(index);

		//! Следующий токен - это закрывающая квадратная скобка
		token = GetNextToken();
		if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightSquareBrace)) // ']'
		{
			throw ParseError("ParseArrayDefinition: Unexpected token type (" + TokenInfo(token) + 
				") - expect token ']'.");
		}
		token = GetNextToken();
	}
	//! Должен быть определен хотя бы 1 индекс
	if(!stmt_ptr->array_idxs_.size())
		throw ParseError("ParseArrayDefinition: array_idxs_.size() == 0.");
	
	//! Инициализация массива
	token = GetToken(); // '=' или ';'
	if(token.type == Transalg::ttOperator && token.code == Transalg::tcAssign)
	{
		token = GetNextToken();
		if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace) // '{'
		{
			ParseArrayInitialize(stmt_ptr->array_idxs_, 0, stmt_ptr->init_values_);
			token = GetNextToken(); // ';'
		}
		else
		{
			stmt_ptr->init_expression_ = ParseExpression();
			token = GetToken();
		}
	}

	//! Заключительный токен - это ';'
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcSemicolumn))
	{
		throw ParseError("ParseArrayDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token ';'.");
	}

	return stmt_ptr;
}

void Parser::Impl::ParseArrayInitialize(std::vector<unsigned>& indexes, unsigned level, std::vector<unsigned>& init_values)
{
	//! Начальный токен - открывающая скобка
	Token token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace)) // '{'
	{
		throw ParseError("ParseArrayInitialize: Unexpected token type (" + TokenInfo(token) + ") - expect token '{'.");
	}
	++level;
	unsigned size(0);
	if(indexes.size() == level)
	{
		token = GetNextToken();
		while(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber)
		{
			++size;
			const unsigned value = boost::lexical_cast<unsigned>(token.text);
			init_values.push_back(value);
			//! Следующий допустимый токен - это ',' или '}'
			token = GetNextToken();
			if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcComma) // ','
			{
				token = GetNextToken();
			}
		}
	}
	//! Разбираем вложенные подмассивы
	else
	{
		token = GetNextToken();
		while(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace) // '{'
		{
			++size;
			ParseArrayInitialize(indexes, level, init_values);
			//! Следующий допустимый токен - это ',' или '}'
			token = GetNextToken();
			if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcComma) // ','
			{
				token = GetNextToken();
			}
		}
	}
	// warning
	assert(size == indexes.at(level - 1));
	//! Конечный токен - закрывающая скобка
	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightBrace)) // '}'
	{
		throw ParseError("ParseArrayInitialize: Unexpected token type (" + TokenInfo(token) + ") - expect token '}'.");
	}
}

StatementFunctionDefinitionPtr Parser::Impl::ParseFunctionDefinition(const Token& data_type, const Token& id)
{
	const DataType dtype = GetDataType(data_type.code);
	StatementFunctionDefinitionPtr stmt_ptr = 
		boost::make_shared<StatementFunctionDefinition>(current_context_, data_type.line, dtype, id.text);

	//! Разбор параметров функции 
	//! Начальный токен - открывающая круглая скобка
	Token token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)) // '('
	{
		throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token '('.");
	}

	token = GetNextToken();
	unsigned param_count(0);
	while(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightRoundBrace)) // ')'
	{
		//! Пропускаем запятые
		if(param_count > 0)
		{
			if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcComma)) // ','
			{
				throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token ','.");
			}
			token = GetNextToken();
		}
		else
			token = GetToken();

		//! Тип данных
		if(token.type != Transalg::ttDataType)
			throw ParseError("ParseFunctionDefinition: (" + TokenInfo(token) + ") - expect data type.");

		stmt_ptr->args_type_.push_back(GetDataType(token.code));

		//! Идентификатор
		token = GetNextTokenId();
		stmt_ptr->args_id_.push_back(token.text);
		
		//! Индексы массива (опционально)
		stmt_ptr->arg_indexes_.push_back(std::vector<unsigned>());
		token = GetNextToken(); // '[', ',', ')'
		while(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace) // '['
		{
			std::vector<unsigned>& indexs = stmt_ptr->arg_indexes_.back();
			token = GetNextTokenNumber();
			indexs.push_back(boost::lexical_cast<unsigned>(token.text));
			token = GetNextToken();
			if(!(token.type == Transalg::ttDelimiter && token.code && Transalg::tcRightSquareBrace)) // ']'
			{
				throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token ']'.");
			}
			token = GetNextToken(); // '[', ',', ')'
		}
		++param_count;
	}

	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightRoundBrace)) // ')'
		throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token ')'.");

	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace)) // '{'
		throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token '{'.");
		
	//! Разбор тела функции
	stmt_ptr->block_ = ParseStatementBlock(stmt_ptr.get());

	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightBrace))// '}
		throw ParseError("ParseFunctionDefinition: Unexpected token type (" + TokenInfo(token) + ") - expect token '}'.");

	return stmt_ptr;
}

StatementBlockPtr Parser::Impl::ParseStatementBlock(StatementFunctionDefinition* func_ptr)
{
	Token token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace)) // '{'
	{
		throw ParseError("ParseStatementBlock: Unexpected token type (" + TokenInfo(token) + ") - expect token '{'.");
	}
	StatementBlockPtr block_ptr = boost::make_shared<StatementBlock>(current_context_, token.line, func_ptr);
	//! Создаем новый контекст - текущий блок инструкций
	current_context_ = block_ptr.get();
	token = GetNextToken();
	while(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightBrace)) // '}'
	{
		block_ptr->statements_.push_back(ParseStatement());
		token = GetNextToken();
	}
	
	return block_ptr;
}

StatementIfPtr Parser::Impl::ParseStatementIf()
{
	Token token = GetToken();
	assert(token.type == Transalg::ttKeyWord && token.code == Transalg::tcIf);

	StatementIfPtr stmt_ptr = boost::make_shared<StatementIf>(current_context_, token.line);

	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)) // '('
		throw ParseError("ParseStatementIf: Unexpected token type (" + TokenInfo(token) + ") - expect token '('.");

	//! Начинаем разбор выражения
	GetNextToken();
	stmt_ptr->condition_ = ParseExpression();

	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightRoundBrace)) // ')'
		throw ParseError("ParseStatementIf: Unexpected token type (" + TokenInfo(token) + ") - expect token ')'.");

	//! true-branch (всегда есть)
	GetNextToken();
	stmt_ptr->true_branch_ = ParseStatement();
	
	//! false-branch (может не быть)
	Token ahead_token = GetLookAheadToken();
	if(ahead_token.type == Transalg::ttKeyWord && ahead_token.code == Transalg::tcElse)
	{
		GetNextToken(); // 'else'
		GetNextToken(); // первый токен инструкции
		stmt_ptr->false_branch_ = ParseStatement();
	}

	return stmt_ptr;
}

StatementForPtr Parser::Impl::ParseStatementFor()
{
	Token token = GetToken();
	assert(token.type == Transalg::ttKeyWord && token.code == Transalg::tcFor);

	StatementForPtr stmt_ptr = boost::make_shared<StatementFor>(current_context_, token.line);

	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)) // '('
		throw ParseError("ParseStatementFor: Unexpected token type (" + TokenInfo(token) + ") - expect token '('.");

	//! Инициализирующее выражение цикла
	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcSemicolumn)) // ';'
	{
		stmt_ptr->init_ = ParseStatement();
		//! Инициализирующее выражение может быть либо выражением ТА-программы, либо объявлением переменной
		Transalg::StatementType stmt_type = stmt_ptr->init_->GetStatementType();
		if(stmt_type != Transalg::stmtExpression && stmt_type != Transalg::stmtVarDefinition)
			throw ParseError("ParseStatementFor: initial statement must be a variable definition statement or expression.");
	}
	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcSemicolumn)) // ';'
		throw ParseError("ParseStatementFor: Unexpected token type (" + TokenInfo(token) + ") - expect token ';'.");

	//! Условие цикла
	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcSemicolumn)) // ';'
	{
		stmt_ptr->condition_ = ParseExpression();
	}
	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcSemicolumn)) // ';'
		throw ParseError("ParseStatementFor: Unexpected token type (" + TokenInfo(token) + ") - expect token ';'.");

	//! Выражение, вычисляемое на каждом шаге цикла
	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)) // ')'
	{
		stmt_ptr->step_ = ParseExpression();
	}
	token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightRoundBrace)) // ')'
		throw ParseError("ParseStatementFor: Unexpected token type (" + TokenInfo(token) + ") - expect token ')'.");

	//! Тело цикла
	token = GetNextToken();
	stmt_ptr->body_ = ParseStatement();

	return stmt_ptr;
}

StatementTablePtr Parser::Impl::ParseTableFunction()
{
	Token token = GetToken();
	assert(token.type == Transalg::ttKeyWord && token.code == Transalg::tcTable);

	//! Идентификатор
	Token token_table_id = GetNextTokenId();

	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace)) // '['
	{
		throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token '['.");
	}

	//! Число входных параметров и их размер
	std::vector<unsigned> input_bit_size;
	token = GetNextToken();
	while(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber)
	{
		input_bit_size.push_back(boost::lexical_cast<unsigned>(token.text));
	}

	if(input_bit_size.size() == 0)
		throw ParseError("ParseStatementTable: input_bit_size.size() == 0.");

	//! Разделитель ':'
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcColon)) // ':'
	{
		throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token ':'.");
	}

	//! Число выходных параметров и их размер
	std::vector<unsigned> output_bit_size;
	token = GetNextToken();
	while(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber)
	{
		output_bit_size.push_back(boost::lexical_cast<unsigned>(token.text));
	}

	if(output_bit_size.size() == 0)
		throw ParseError("ParseStatementTable: output_bit_size.size() == 0.");

	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightSquareBrace)) // ']'
	{
		throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token ']'.");
	}

	StatementTablePtr stmt_ptr = boost::make_shared<StatementTable>(current_context_, token.line, token_table_id.text);
	stmt_ptr->input_bit_size_ = input_bit_size;
	stmt_ptr->output_bit_size_ = output_bit_size;
	
	//! Тело табличной функции
	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftBrace)) // '{'
		throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token '{'.");

	token = GetNextToken();
	while(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace) // '['
	{
		StatementTable::TableRow row(input_bit_size.size(), output_bit_size.size());
		//! Вход
		for(unsigned i = 0; i < input_bit_size.size(); ++i)
		{
			token = GetNextToken();
			if(!(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber))
				throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect decimal integer.");
			row.in_values[i] = boost::lexical_cast<unsigned>(token.text);
		}

		//! Разделитель
		token = GetNextToken();
		if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcColon)) // ':'
			throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token ':'.");

		//! Выход
		for(unsigned i = 0; i < output_bit_size.size(); ++i)
		{
			token = GetNextToken();
			if(!(token.type == Transalg::ttNumber && token.code == Transalg::tcDecNumber))
				throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect decimal integer.");
			row.out_values[i] = boost::lexical_cast<unsigned>(token.text);
		}

		token = GetNextToken();
		if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightSquareBrace)) // ']'
			throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token ']'.");

		stmt_ptr->table_.push_back(row);
		token = GetNextToken(); // '[' (следующая строка) или '}'
	}

	//! Закрывающая фигурная скобка
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightBrace)) // '}'
		throw ParseError("ParseStatementTable: Unexpected token type (" + TokenInfo(token) + ") - expect token '}'.");
	
	return stmt_ptr;
}

Attribute Parser::Impl::GetAttribute(Transalg::TokenCode code) const
{
	switch(code)
	{
	case Transalg::tcIn: 
		return AttributeIn;
	case Transalg::tcOut: 
		return AttributeOut;
	case Transalg::tcMem: 
		return AttributeMem;
	}
	return AttributeNone;
}

DataType Parser::Impl::GetDataType(Transalg::TokenCode code) const
{
	switch(code)
	{
	case Transalg::tcVoid:
		return dtVoid;
	case Transalg::tcInt:
		return dtInt;
	case Transalg::tcBit:
		return dtBit;
	}
	throw std::invalid_argument("GetDataType: incorrect TokenCode.");
	return dtVoid;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Expression Parser

//! Приоритеты операций языка ТА
int GetOperatorPriority(TokenCode code)
{
	switch(code)
	{
		//! Открывающие скобки
		case Transalg::tcLeftSquareBrace:
		case Transalg::tcLeftRoundBrace:   return 0;
		//! Закрывающие скобки
		case Transalg::tcRightSquareBrace:
		case Transalg::tcRightRoundBrace:  return 1;
		//! Присваивание
		case Transalg::tcAssign:           return 2;
		//! Конкатенация
		case Transalg::tcConcat:           return 3;
		//! Дизъюкция
		case Transalg::tcOr:               return 4;
		//! Исключающее ИЛИ
		case Transalg::tcXor:              return 5;
		//! Конъюнкция
		case Transalg::tcAnd:              return 6;
		//! Операции сравнения
		case Transalg::tcEqual:
		case Transalg::tcNotEqual:         return 7;
		case Transalg::tcGreat:
		case Transalg::tcGreatEq:
		case Transalg::tcLess:
		case Transalg::tcLessEq:           return 8;
		//! Операции побитового сдвига
		case Transalg::tcLeftShift:
		case Transalg::tcRightShift:
		case Transalg::tcLeftSaveShift:
		case Transalg::tcRightSaveShift:
		case Transalg::tcLeftCircleShift:
		case Transalg::tcRightCircleShift: return 9;
		//! Арифметические операции
		case Transalg::tcSum:
		case Transalg::tcSub:              return 10;
		case Transalg::tcMul:
		case Transalg::tcDiv:
		case Transalg::tcMod:              return 11;
		//! Отрицание
		case Transalg::tcNot:              return 12;
		default: 
			throw ParseError("GetOperatorPriority: Unknown operator.");
	}
}

unsigned Parser::Impl::ParseFunctionCallArguments(std::vector<ExpressionOperand>& poliz)
{
	//! Текущий токен - это левая открывающая круглая скобка
	Token token = GetToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)) // '('
		throw ParseError("ParseFunctionCallArguments: Unexpected token type (" + TokenInfo(token) + ") - expect token '('.");

	//! Осуществляем разбор аргументов функции, если они есть
	unsigned arg_count(0);
	token = GetNextToken();
	if(!(token.type == Transalg::ttDelimiter && token.code == Transalg::tcRightRoundBrace))
	{
		//! Аргументы функции - это список выражений, перечисленных через запятую
		for(;;)
		{
			++arg_count;
			ExpressionPtr arg = ParseExpression();
			//! Копируем результат в выходную последовательность
			const std::size_t operand_count = arg->GetOperandsCount();
			if(operand_count == 0)
			{
				std::stringstream ss;
				ss << "ParseFunctionCallArguments: invalid argument of function "
					<< "(line = " << token.line << ")";
				throw ParseError(ss.str());
			}

			for(unsigned i = 0; i < operand_count; ++i)
			{
				poliz.push_back(arg->GetOperand(i));
			}
			//! Продолжаем цикл, если текущий токен ','
			if(GetToken().code != Transalg::tcComma)
				break;

			token = GetNextToken();
		}
	}
	//! Убедимся, что дошли до закрывающей круглой скобки
	token = GetToken();
	if(token.code != Transalg::tcRightRoundBrace) // ')'
		throw ParseError("ParseFunctionCallArguments: Unexpected token type (" + TokenInfo(token) + ") - expect token ')'.");

	return arg_count;
}

unsigned Parser::Impl::ParseArrayArguments(std::vector<ExpressionOperand>& poliz)
{
	unsigned arg_count(0);
	Token token = GetToken();
	while(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace)
	{
		++arg_count;
		token = GetNextToken();
		ExpressionPtr arg = ParseExpression();
		//! Копируем результат в выходную последовательность
		const std::size_t operand_count = arg->GetOperandsCount();
		if(operand_count == 0)
			throw ParseError("Parser::ParseArrayArguments: invalid expression of array index.");

		for(unsigned i = 0; i < operand_count; ++i)
		{
			poliz.push_back(arg->GetOperand(i));
		}
		//! Убедимся, что текущий токен - это закрывающаяся квадратная скобка
		token = GetToken();
		if(token.code != Transalg::tcRightSquareBrace) // ']'
		{
			throw ParseError("ParseArrayArguments: Unexpected token type (" + TokenInfo(token) + ") - expect token ']'.");
		}
		token = GetNextToken();
	}
	return arg_count;
}

ExpressionPtr Parser::Impl::ParseExpression()
{
	//! Обеспечиваем сортировку операций в соответствии с их приоритетом
	std::stack<Token> sorter;
	std::vector<ExpressionOperand> poliz;
	Token token = GetToken();
	ExpressionPtr expression = boost::make_shared<Expression>(current_context_, token.line);
	for(;;)
	{
		//! Числа сразу помещаем в выходную последовательность
		if(token.type == Transalg::ttNumber)
		{
			poliz.push_back(ExpressionOperand(Transalg::OperandTypeConst, token.text, 0, token.line));
		}
		//! Идентификатор может быть именованной константой, переменной, массивом или функцией.
		//! По умолчанию считаем его переменной и помещаем в выходную последовательность.
		else if(token.type == Transalg::ttId)
		{
			DefineConstMap::const_iterator define_it = define_map_.find(token.text);
			if(define_it != define_map_.end())
			{
				//! Именованная константа
				poliz.push_back(ExpressionOperand(Transalg::OperandTypeConst, define_it->second, 0, token.line));
			}
			else
			{
				ExpressionOperand id(Transalg::OperandTypeVariable, token.text, 0, token.line);
				token = GetNextToken();

				//! Вызов функции
				if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftRoundBrace)
				{
					id.type_ = Transalg::OperandTypeFunction;
					id.arg_count_ = ParseFunctionCallArguments(poliz);
					token = GetNextToken();
				}
				//! Обращение к элементу массива
				else if(token.type == Transalg::ttDelimiter && token.code == Transalg::tcLeftSquareBrace)
				{
					id.type_ = Transalg::OperandTypeArrayVariable;
					id.arg_count_ = ParseArrayArguments(poliz);
					token = GetToken();
				}
				poliz.push_back(id);
				continue;
			}
		}
		else if(token.type == Transalg::ttDelimiter)
		{
			//! Открывающую круглую скобку помещаем в стек
			if(token.code == Transalg::tcLeftRoundBrace)
			{
				sorter.push(token);
			}
			//! Закpывающая кpуглая скобка выталкивает все опеpации из стека до ближайшей откpывающей скобки, 
			//! Cами скобки в выходную стpоку не пеpеписываются, а уничтожают дpуг дpуга.
			else if(token.code == Transalg::tcRightRoundBrace)
			{
				while(!sorter.empty() && sorter.top().code != Transalg::tcLeftRoundBrace)
				{
					const Token& token = sorter.top();
					poliz.push_back(ExpressionOperand(Transalg::OperandTypeOperator, token.text, (token.code == Transalg::tcNot ? 1 : 2), token.line));
					sorter.pop();
				}
				if(!sorter.empty())
				{
					//! Вытолкнуть открывающую круглую скобку
					sorter.pop();
				}
				//! Если для закрывающей скобки не нашли пары, то выражение закончилось
				//! Выход из внешнего цикла for
				else
					break; 
			}
			//! Любой другой разделитель считается концом выражения
			else
				break;
		}
		else if(token.type == Transalg::ttOperator)
		{
			const int priority = GetOperatorPriority(token.code);
			//! Переместить все операции из стека с большим или равным приоритетом в выходную последовательность
			while(!sorter.empty() && priority <= GetOperatorPriority(sorter.top().code))
			{
				const Token& operand = sorter.top();
				const unsigned arg_count = (operand.code == Transalg::tcNot ? 1 : 2);
				poliz.push_back(ExpressionOperand(Transalg::OperandTypeOperator, operand.text, arg_count, operand.line));
				sorter.pop();
			}
			//! Поместить текущую операцию в стек
			sorter.push(token);
		}
		else
		{
			throw ParseError("ParseExpression: Unexpected token type (" + TokenInfo(token) + ").");
		}

		token = GetNextToken();
	}
	//! Все операторы из стека перенести в выходную последовательность
	while(!sorter.empty())
	{
		const Token& operand = sorter.top();
		const unsigned arg_count = (operand.code == Transalg::tcNot ? 1 : 2);
		poliz.push_back(ExpressionOperand(Transalg::OperandTypeOperator, operand.text, arg_count, operand.line));
		sorter.pop();
	}
	//! Сохраним результат
	expression->operands_.insert(expression->operands_.end(), poliz.begin(), poliz.end());
	return expression;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Token getters

const Token& Parser::Impl::GetToken() const
{
	if(token_cursor_ == tokens_.end())
	{
		throw ParseError("Unexcepted end of program.");
	}
	return *token_cursor_;
}

Token Parser::Impl::GetLookAheadToken() const
{
	if(token_cursor_ == tokens_.end() || (token_cursor_ + 1) == tokens_.end())
	{
		//! empty token
		return Token();
	}
	return *(token_cursor_ + 1);
}

const Token& Parser::Impl::GetNextToken()
{
	if(token_cursor_ == tokens_.end() || ++token_cursor_ == tokens_.end())
	{
		throw ParseError("Unexcepted end of program.");
	}
	return *token_cursor_;
}

const Token& Parser::Impl::GetNextTokenId()
{
	const Token& token = GetNextToken();
	if(token.type != Transalg::ttId)
	{
		throw ParseError(TokenInfo(token) + std::string(" - is not valid identifier."));
	}
	return token;
}

const Token& Parser::Impl::GetNextTokenNumber()
{
	const Token& token = GetNextToken();
	if(token.type != Transalg::ttNumber)
	{
		throw ParseError(TokenInfo(token) + std::string(" - is not valid number."));
	}
	return token;
}

const Token& Parser::Impl::GetNextTokenDatatype()
{
	const Token& token = GetNextToken();
	if(token.type != Transalg::ttDataType)
	{
		throw ParseError(TokenInfo(token) + std::string(" - is not valid data type."));
	}
	return token;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Parser

Parser::Parser()
{
	impl_ = boost::make_shared<Parser::Impl>();
}

StatementBlockPtr Parser::ParseProgram(const std::string& filename)
{
	return impl_->ParseProgram(filename);
}

StatementBlockPtr Parser::ParseProgramText(const std::string& text)
{
	return impl_->ParseProgramText(text);
}

} // namespace Transalg
