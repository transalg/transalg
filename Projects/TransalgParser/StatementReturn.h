#pragma once

#include "Types.h"
#include "Statement.h"
#include "Expression.h"

namespace Transalg
{

class StatementReturn: public Statement
{
	friend class Parser;
public:

	explicit StatementReturn(const StatementBlock* context, unsigned line)
		: Statement(context, stmtReturn, line)
	{}

	ExpressionPtr GetReturnExpression() const;

private:
	//! Выражение, значение которого возвращаем
	ExpressionPtr return_expression_;
};

typedef boost::shared_ptr<StatementReturn> StatementReturnPtr;

} // namespace Transalg
