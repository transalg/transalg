#pragma once

#include "Statement.h"
#include "Expression.h"

namespace Transalg
{

class Serializer
{
public:

	static void SerializeStatement(std::ostream&, const StatementPtr&);
	static void SerializeExpression(std::ostream&, const Expression&);
	static void SerializeStatementVarDefinition(std::ostream&, const StatementVarDefinition&);
	static void SerializeStatementArrayDefinition(std::ostream&, const StatementArrayDefinition&);
	static void SerializeStatementFunctionDefinition(std::ostream&, const StatementFunctionDefinition&);
	static void SerializeStatementBlock(std::ostream&, const StatementBlock&);
	static void SerializeStatementFor(std::ostream&, const StatementFor&);
	static void SerializeStatementIf(std::ostream&, const StatementIf&);
	static void SerializeStatementReturn(std::ostream&, const StatementReturn&);
	static void SerializeStatementTable(std::ostream&, const StatementTable&);

};

} // namespace Transalg
