#include "stdafx.h"
#include "../TruthTable.h"

namespace Transalg
{

TruthTable::TruthTable(std::string id, std::size_t input_size, std::size_t output_size)
	: id_(id)
	, input_size_(input_size)
	, output_size_(output_size)
{}

const std::string& TruthTable::GetId() const
{
	return id_;
}

std::size_t TruthTable::GetInputSize() const
{
	return input_size_;
}

std::size_t TruthTable::GetOutputSize() const
{
	return output_size_;
}

std::size_t TruthTable::GetTableSize() const
{
	return table_.size();
}

TruthTable::RowContainer::const_iterator TruthTable::Begin() const
{
	return table_.begin();
}

TruthTable::RowContainer::const_iterator TruthTable::End() const
{
	return table_.end();
}

TruthTable::Data TruthTable::GetValue(TruthTable::Data input) const
{
	RowContainer::const_iterator it = table_.find(input);
	return (it == table_.end() ? static_cast<Data>(0) : it->second);
}

} // namespace Transalg
