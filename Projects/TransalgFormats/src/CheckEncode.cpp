#include "stdafx.h"
#include "../EncodeFactory.h"

namespace Transalg
{

namespace
{

bool IsNull(const LogicVarPtr& ptr)
{
	return ptr == 0;
}

size_t CheckNullPointer(LogicVarContainer& vars)
{
	LogicVarContainer::iterator it = std::remove_if(vars.begin(), vars.end(), IsNull);
	const size_t removed = vars.end() - it;
	vars.resize(it - vars.begin());
	return removed;
}

size_t CheckVarsRedefinition(const LogicVarContainer& vars, std::set<VarId>& defined_vars, std::ostream& log)
{
	size_t err_cnt(0);
	for(unsigned i = 0; i < vars.size(); ++i)
	{
		const VarId var_id(vars[i]->var_id);
		if(defined_vars.find(var_id) != defined_vars.end())
		{
			log << "Redefinition of variable: var_id = " << var_id << "." << std::endl;
			++err_cnt;
			continue;
		}
		defined_vars.insert(var_id);
	}
	return err_cnt;
}

size_t CheckVarDefinitionOrder(const LogicVarContainer& vars, std::ostream& log)
{
	size_t error_cnt(0);
	for(unsigned i(0); i < vars.size(); ++i)
	{
		if(!vars[i])
			continue;
		if(vars[i]->var_value)
		{
			const VarId var_id(vars[i]->var_id);
			const LogicFormula& formula(*(vars[i]->var_value));
			LogicVarSet var_set;
			formula.GetVariableSet(var_set);
			for(LogicVarSet::const_iterator var_it = var_set.begin();
				var_it != var_set.end();
				++var_it)
			{
				const LogicVar& var(**var_it);
				if(var.var_id >= var_id)
				{
					log << "[ERROR]: Incorrect dependence: variable x" << var_id 
						<< " is defined by variable x" << var.var_id << "." << std::endl;
					++error_cnt;
					break;
				}
			}
		}
	}
	return error_cnt;
}

typedef std::map<VarId, unsigned> VarCounterMap;

void FillVarCounterMap(VarCounterMap& vc, const LogicVarContainer& vars, std::ostream& log)
{
	for(unsigned i(0); i < vars.size(); ++i)
	{
		const LogicVar& var(*(vars[i]));
		vc[var.var_id] = 0;

		if(!var.var_value)
			continue;

		const LogicFormula& formula(*var.var_value);
		LogicVarSet var_set;
		formula.GetVariableSet(var_set);
		for(LogicVarSet::const_iterator var_it = var_set.begin();
			var_it != var_set.end();
			++var_it)
		{
			const LogicVar& var(**var_it);
			if(vc.find(var.var_id) == vc.end())
			{
				log << "[WARNING]: FillVarCounterMap: variable x" << var.var_id << " is not found in map." << std::endl;
				continue;
			}
			vc[var.var_id]++;
		}
	}
}

void FillVarCounterMap(VarCounterMap& vc, const LogicFormulaContainer& formulae, std::ostream& log)
{
	for(unsigned i(0); i < formulae.size(); ++i)
	{
		if(!formulae[i]) continue;
		const LogicFormula& formula(*formulae[i]);
		LogicVarSet var_set;
		formula.GetVariableSet(var_set);
		for(LogicVarSet::const_iterator var_it = var_set.begin();
			var_it != var_set.end();
			++var_it)
		{
			const LogicVar& var(**var_it);
			if(vc.find(var.var_id) == vc.end())
			{
				log << "[WARNING]: FillVarCounterMap: variable x" << var.var_id << " is not found in map." << std::endl;
				continue;
			}
			vc[var.var_id]++;
		}
	}
}

size_t RemoveUnusedVars(const VarCounterMap& vc, LogicVarContainer& vars)
{
	size_t i(0), j(0);
	while(i < vars.size())
	{
		const VarId var_id(vars[i]->var_id);
		VarCounterMap::const_iterator var_it = vc.find(var_id);
		if(var_it == vc.end())
		{
			continue;
		}
		//! Сохраняем переменные, которые используются
		if(var_it->second > 0)
		{
			vars[j] = vars[i];
			++j;
		}
		++i;
	}
	vars.resize(j);
	return (i - j);
}

size_t RemoveUndefinedVars(LogicVarContainer& vars)
{
	size_t i(0), j(0);
	for(; i < vars.size(); ++i)
	{
		//! Сохраняем переменные, значения которых определены
		if(vars[i] && vars[i]->var_value)
		{
			vars[j] = vars[i];
			++j;
		}
	}
	vars.resize(j);
	return (i - j);
}

size_t CheckInputVars(LogicVarContainer& vars, std::ostream& log)
{
	size_t error_cnt(0);
	for(unsigned i(0); i < vars.size(); ++i)
	{
		LogicVarPtr var(vars[i]);
		if(!var)
		{
			log << "[ERROR]: var - null pointer (var index: " << i << ")" << std::endl;
			++error_cnt;
			continue;
		}
		if(var->var_type != LogicVarInput)
		{
			log << "[ERROR]: invalid type of input variable x" << var->var_id << "." << std::endl;
			++error_cnt;
		}
		if(var->var_value)
		{
			log << "[WARNING]: value of input variable x" << var->var_id << " must be null." << std::endl;
			++error_cnt;
		}
	}
	return error_cnt;
}

size_t CheckOutputVars(LogicVarContainer& vars, std::ostream& log)
{
	size_t error_cnt(0);
	for(unsigned i(0); i < vars.size(); ++i)
	{
		LogicVarPtr var(vars[i]);
		if(!var)
		{
			log << "[ERROR]: var - null pointer (var index: " << i << ")" << std::endl;
			++error_cnt;
			continue;
		}
		if(var->var_type != LogicVarOutput)
		{
			log << "[ERROR]: invalid type of output variable x" << var->var_id << "." << std::endl;
			++error_cnt;
		}
		if(!var->var_value)
		{
			log << "[WARNING]: value of output variable x" << var->var_id << " is not defined." << std::endl;
			++error_cnt;
		}
	}
	return error_cnt;
}

size_t CheckTseitinVars(LogicVarContainer& vars, std::ostream& log)
{
	size_t error_cnt(0);
	for(unsigned i(0); i < vars.size(); ++i)
	{
		LogicVarPtr var(vars[i]);
		if(!var)
		{
			log << "[ERROR]: var - null pointer (var index: " << i << ")" << std::endl;
			++error_cnt;
			continue;
		}
		if(var->var_type != LogicVarTseitin)
		{
			log << "[ERROR]: invalid type of tseitin variable x" << var->var_id << "." << std::endl;
			++error_cnt;
		}
		if(!var->var_value)
		{
			log << "[ERROR]: value of tseitin variable x" << var->var_id << " is not defined." << std::endl;
			++error_cnt;
		}
	}
	//! Проверить порядок определения цейтиновских переменных:
	//! каждая цейтиновская переменная определяется через ранее введенные переменные.
	error_cnt += CheckVarDefinitionOrder(vars, log);
	return error_cnt;
}

void VerifyVarSetSize(std::ostream& log, 
					  const LogicVarContainer& vars, 
					  std::map<size_t, size_t>& hist,
					  size_t max_var_set_size)
{
	for(size_t i(0); i < vars.size(); ++i)
	{
		const LogicVarPtr& var(vars[i]);
		if(!var) continue;
		if(!var->var_value) continue;
		const LogicFormulaPtr& formula(var->var_value);
		LogicVarSet var_set;
		formula->GetVariableSet(var_set);
		const size_t var_count(var_set.size() + 1);
		hist[var_count]++;
		//! В лог выводим формулы, определенные над большим числом переменных
		if(var_count > max_var_set_size)
		{
			log << *var;
		}
	}
}

} // anonimous namespace

void EncodeFactory::CheckEncode(std::ostream& log)
{
	//! Проверить и удалить пустые указатели и неиспользуемые переменные
	RemoveUnusedVariables(log);

	size_t error_cnt(0);
	//! Проверить, есть ли переопределенные переменные
	{
		std::set<VarId> ids;
		error_cnt += CheckVarsRedefinition(input_vars, ids, log);
		error_cnt += CheckVarsRedefinition(tseitin_vars, ids, log);
		error_cnt += CheckVarsRedefinition(output_vars, ids, log);
	}

	error_cnt += CheckInputVars(input_vars, log);
	error_cnt += CheckTseitinVars(tseitin_vars, log);
	error_cnt += CheckOutputVars(output_vars, log);

	log << "Total count of error: " << error_cnt << std::endl;
}

void EncodeFactory::RemoveUnusedVariables(std::ostream& log)
{
	size_t removed(0);
	//! Проверить пустые указатели и удалить их, если есть
	removed = CheckNullPointer(input_vars);
	log << "Input vars: " << removed << " null pointer removed." << std::endl;
	removed = CheckNullPointer(tseitin_vars);
	log << "Tseitin vars: " << removed << " null pointer removed." << std::endl;
	removed = CheckNullPointer(output_vars);
	log << "Output vars: " << removed << " null pointer removed." << std::endl;

	//! Удалить из кодировки лишние переменные
	removed_var_count_ = 0;
	do
	{
		//! Посчитаем число использований каждой переменной
		VarCounterMap vc;
		FillVarCounterMap(vc, input_vars, log);
		FillVarCounterMap(vc, tseitin_vars, log);
		FillVarCounterMap(vc, output_vars, log);
		FillVarCounterMap(vc, constraints, log);

		//! Удалим из кодировки неиспользуемые входные и цейтиновские переменные
		removed = 0;
		removed += RemoveUnusedVars(vc, input_vars);
		removed += RemoveUnusedVars(vc, tseitin_vars);
		removed_var_count_ += removed;
	}
	while(removed > 0);

	//! Удалим неопределенные выходные переменные
	removed_var_count_ += RemoveUndefinedVars(output_vars);

	log << "Remove unused variables: " << removed_var_count_ << std::endl;
}

size_t EncodeFactory::GetRemovedVariablesCount() const
{
	return removed_var_count_;
}

std::string EncodeFactory::CheckVarSetSize(std::ostream& log) const
{
	std::stringstream out;
	std::map<size_t, size_t> hist;
	VerifyVarSetSize(log, tseitin_vars, hist, max_var_set_size_);
	VerifyVarSetSize(log, output_vars, hist, max_var_set_size_);

	const size_t csz(15);
	out << std::setw(csz) << "Var set size" << std::setw(csz) << "N formulas" << std::endl;
	for(std::map<size_t, size_t>::const_iterator it = hist.begin();
		it != hist.end();
		++it)
	{
		out << std::setw(csz) << it->first << std::setw(csz) << it->second << std::endl;
	}
	return out.str();
}

} // namespace Transalg
