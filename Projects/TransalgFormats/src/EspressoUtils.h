#pragma once

namespace Transalg
{

//! Таблица истинности
struct Table
{
	Table(): data(0), rows(0), cols(0), alloc_rows(0), alloc_cols(0)
	{}

	Table(size_t x, size_t y);

	~Table();

	/**
	* @brief Изменить число строк, которые используются в таблице (rows).
	* Число используемых строк таблицы не может быть больше, чем их выделено в памяти (alloc_rows).
	* @param used_rows [in] - устанавливаемое число строк.
	*/
	void ResetRows(size_t used_rows);

	/**
	* @brief Изменить число столбцов, которые используются в таблице (cols).
	* Число используемых столбцов таблицы не может быть больше, чем их выделено в памяти (alloc_cols).
	* @param used_cols [in] - устанавливаемое число столбцов.
	*/
	void ResetCols(size_t used_cols);

	char** data;
	//! Реально используемое число строк/столбцов
	size_t rows;
	size_t cols;
	//! Выделенное число строк/столбцов
	size_t alloc_rows;
	size_t alloc_cols;
};

/**
* @brief Минимизация булевой формулы при помощи алгоритма Espresso.
* @param table_in [in] - строки таблицы истинности, на которых минимизируемая формула принимает значение истина.
* @param table_out [out] - таблица, описывающая результат минимизации формулы.
* @return Возвращает код ошибки (0 если все хорошо).
*/
int Minimize(const Table& table_in, Table& table_out);

/**
* @brief Вывод результата в поток.
* @param out [in] - ссылка на поток, куда выводим результаты.
* @param table [in] - таблица истинности.
*/
void PrintTable(std::ostream& out, const Table& table);

/**
* @brief Записать таблицу в PLA-файл. 
* Таблица может потом использоваться в качестве входных данных Espresso.
* Только для таблиц булевых функций.
* @param filename [in] Имя файла, в который записываем таблицу.
* @param table [in] Строки таблицы истинности, на которых минимизируемая формула принимает значение истина.
*/
void WritePla(const char* filename, const Table& table);

/**
* @brief Выводим таблицу в поток в DIMACS формате.
* @param out [in] - поток, в который выводим данные.
* @param table [in] - таблица, содержащая данные.
* @param var_numbers [in] - номера переменных.
* @param disjuncts [out] - возвращает число диъюнктов в КНФ.
* @param literals [out] - возврвщает число литералов в КНФ.
*/
void WriteCnfDimacs(std::ostream& out, const Table& table, const std::vector<unsigned>& var_numbers, 
					size_t& disjuncts, size_t& literals);

} // namespace Transalg
