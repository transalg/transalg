#pragma once
#include "Types.h"

namespace Transalg
{

/**
* \brief Представление слоев переменных.
*/
class VarLayers
{
public:
	
	VarLayers(const LogicVarContainer& vars);

	/**
	* \brief Число слоев.
	*/
	std::size_t GetLayerCount() const;

	/**
	* \brief Число переменных.
	*/
	std::size_t GetVarCount() const;

	/**
	* \brief Получить переменные слоя по его номеру.
	* \param index [in] Номер слоя.
	*/
	const VarIdContainer& GetLayer(unsigned index) const;

	/**
	* \brief Вывести содержимое слоев в выходной поток.
	*/
	void WriteOutLayers(std::ostream& out) const;

	/**
	* \brief Вывести содержимое слоев в файл с КНФ (в качестве комментариев).
	*/
	void WriteLayersDimacs(std::ostream& out) const;

	/**
	* \brief Вывести порядок угадывания переменных в выходной поток.
	* Порядок угадывания соответствует сформированным слоям.
	*/
	void WriteOutVarAssigns(std::ostream& out) const;

	/**
	* \brief Вывести дистанция между соседними слоями.
	*/
	void WriteOutLayersDistance(std::ostream& out) const;

private:

	typedef std::vector<VarIdContainerPtr> Layers;
	Layers layers_;
	std::vector<int> layers_distance_;
	std::size_t var_count_;
};

} // namespace Transalg
