#pragma once

#include "Types.h"
#include <boost/unordered_map.hpp>

namespace Transalg
{

class EncodeFactory
{
public:

	EncodeFactory();

	//! Булевы константы, существуют в единственном экземпляре
	static LogicFormulaPtr True();
	static LogicFormulaPtr False();
	static const LogicOperand& TrueOperand();
	static const LogicOperand& FalseOperand();
	
	//! Число созданных переменных кода
	unsigned GetEncodeVarCount() const;

	//! Очистить кодировку
	void ClearEncode();

	//! Проверка кодировки на корректность
	void CheckEncode(std::ostream& log = std::cout);

	//! Проверить мощности множеств переменых, над которыми построены формулы
	std::string CheckVarSetSize(std::ostream& log) const;
	
	//! Удалить с кодировки не используемые переменные
	void RemoveUnusedVariables(std::ostream& log = std::cout);

	//! Перенумерация переменных в соответствии с их типом
	void RenumberingVariables();

	size_t GetRemovedVariablesCount() const;

public:

	//! Создание новых переменных кода
	LogicVarPtr CreateVar(LogicFormulaPtr value = LogicFormulaPtr());
	LogicVarPtr CreateVar(LogicVarType type, LogicFormulaPtr value = LogicFormulaPtr());

	//! Создание формулы на основе новой переменной кода
	LogicFormulaPtr CreateVarFormula(LogicFormulaPtr var_value = LogicFormulaPtr());
	LogicFormulaPtr CreateVarFormula(LogicVarType var_type, LogicFormulaPtr var_value = LogicFormulaPtr());

	//! Создает новую переменную для формулы formula, только если formula - это булева функция над переменными кода
	//! Если formula - это константа или переменная кода, то она возвращается в неизменном виде
	LogicFormulaPtr CreateVarIfFunction(LogicFormulaPtr formula);

	//! Элементарные булевы функции
	static LogicFormulaPtr And(LogicFormulaPtr x, LogicFormulaPtr y);
	static LogicFormulaPtr Or(LogicFormulaPtr x, LogicFormulaPtr y);
	static LogicFormulaPtr Xor(LogicFormulaPtr x, LogicFormulaPtr y);
	static LogicFormulaPtr Not(LogicFormulaPtr x);
	static LogicFormulaPtr Eq(LogicFormulaPtr x, LogicFormulaPtr y);
	static LogicFormulaPtr Majority(LogicFormulaPtr x, LogicFormulaPtr y, LogicFormulaPtr z);

public:

	//! Множество переменных кода
	LogicVarContainer input_vars;
	LogicVarContainer tseitin_vars;
	LogicVarContainer output_vars;

	//! Ядровые переменные
	LogicVarSet core_vars;

	//! Пропозициональная кодировка (множество ограничений над переменными кода)
	LogicFormulaContainer constraints;

	struct CompareFormula: public std::binary_function<LogicFormula, LogicFormula, bool>
	{
		bool operator()(const LogicFormula& c1, const LogicFormula& c2) const
		{
			return c1 == c2;
		}
	};

	struct HashFormula: public std::unary_function<LogicFormula, size_t>
	{
		size_t operator()(const LogicFormula& formula) const
		{
			return formula.HashCode();
		}
	};

	//! Словарь термов
	typedef boost::unordered_map<LogicFormula, LogicVarPtr, HashFormula, CompareFormula > TermMap;
	TermMap terms_;

private:

	static const std::string var_id_;
	//! Булевы константы существуют в единственном экземпляре
	static const LogicFormulaPtr true_;
	static const LogicFormulaPtr false_;

	static const LogicOperand true_operand_;
	static const LogicOperand false_operand_;

	static const LogicOperand not_operand_;
	static const LogicOperand and_operand_;
	static const LogicOperand or_operand_;
	static const LogicOperand xor_operand_;
	static const LogicOperand equiv_operand_;

	//! Максимальное число переменных, над которыми может быть построена одна формула
	static const size_t max_var_set_size_;

	//! Счетчик созданных переменных кода
	unsigned encode_var_count_;

	size_t removed_var_count_;
};

typedef boost::shared_ptr<EncodeFactory> EncodeFactoryPtr;

} // namespace Transalg
