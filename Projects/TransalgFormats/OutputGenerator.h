#pragma once

#include "Types.h"

namespace Transalg
{

enum OutputFormat
{
	OutputFormatCnf, 
	OutputFormatDnf,
	OutputFormatAnf,
	OutputFormatAig,
	OutputFormatLs,
	OutputFormatLsPostfix
};

class OutputGenerator
{
public:

	OutputGenerator();

	//! Добавление текстовых комментариев в шапку выходного файла
	void AddComment(const std::string& comment);
	void AddComment(const char* comment);

	//! Добавление новых уравнений в систему
	void AddEncode(const LogicVarContainer& vars);
	void AddEncode(const LogicFormulaContainer& constraints);
	void SetCoreVars(const LogicVarSet& vars);

	//! Добавить информацию к переменных по слоям
	void ShowLayerVars(bool show);

	//! Генерация итоговой кодировки
	void Generate(const std::string& filename, OutputFormat format, bool xor_encode = false);

	size_t GetVarCount() const;
	size_t GetDisjunctCount() const;
	size_t GetLiteralsCount() const;

private:

	void WriteOutCnf(std::ostream& out);
	void WriteOutCnf(std::ostream& out, const LogicVarPtr& encode);
	void WriteOutCnf(std::ostream& out, const LogicFormulaPtr& formula);

	void WriteOutLs(std::ostream& out) const;

	void Reset();
	
	void SetVarValues(VarValueMap& value_map, unsigned values) const;

private:

	std::size_t var_count_;
	std::size_t disjunct_count_;
	std::size_t literals_count_;
	std::size_t xor_clause_count_;
	std::size_t input_var_count_;
	std::size_t output_var_count_;

	std::vector<std::string> comments_;

	std::set<VarId> core_vars_;
	LogicVarContainer encode_vars_;
	LogicFormulaContainer encode_constraints_;

	bool xor_encode_;
	bool show_layers_;
};

} // namespace Transalg
