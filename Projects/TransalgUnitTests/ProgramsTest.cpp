#include "stdafx.h"
#include <TransalgParser/Parser.h>
#include <TransalgParser/ParseError.h>
#include <TransalgInterpreter/Interpreter.h>
#include <TransalgFormats/OutputGenerator.h>

namespace
{

//! Относительный путь до файлов с тестами (относительно папки с бинарником)
const std::string g_path("../../Projects/TransalgUnitTests/Tests/");

bool ExecuteProgram(const std::string& input_file, std::string& result, std::size_t encode_word_size)
{
	bool ok(true);
	try
	{
		using namespace Transalg;

		Parser parser;
		StatementBlockPtr program_block = parser.ParseProgram(input_file);

		EncodeFactoryPtr encode = boost::make_shared<EncodeFactory>();
		Interpreter interpreter;
		if(!interpreter.TranslateProgram(program_block, encode))
			return false;

		//! Представляем значение переменных выхода в виде 16-ричной строки
		result = Transalg::LogicEquationSerializer::GetHexStringEncode(encode->output_vars, encode_word_size);
	}
	catch (const Transalg::ParseError& e)
	{
		std::cerr << "PARSE ERROR: " << e.what() << std::endl;
		ok = false;
	}
	catch (const std::exception& e)
	{
		std::cerr << "ERROR: " << e.what() << std::endl;
		ok = false;
	}
	return ok;
}

}

//! Трансляция программы для генератора А5/1
TEST(TA_Programs, A5_1_ProgramTest)
{
	const std::string input_file = g_path + std::string("A5_1_test.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("c7468914f0cb912d16e8a9b81396744c", result);
}

//! http://ru.wikipedia.org/wiki/MD5
//! MD5-хеш от строки "md5"
TEST(TA_Programs, MD5_smd5_test)
{
	const std::string input_file = g_path + std::string("MD5_smd5.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("1bc29b36f623ba82aaf6724fd3b16718", result);
}

//! MD-5 хеш от Message 1 из http://marc-stevens.nl/research/md5-1block-collision/
TEST(TA_Programs, MD5_stevens_msg1_padding_test)
{
	const std::string input_file = g_path + std::string("MD5_stevens_msg1_padding.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("008ee33a9d58b51cfeb425b0959121c9", result);
}

//! MD-5 хеш от Message 2 из http://marc-stevens.nl/research/md5-1block-collision/
TEST(TA_Programs, MD5_stevens_msg2_padding_test)
{
	const std::string input_file = g_path + std::string("MD5_stevens_msg2_padding.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("008ee33a9d58b51cfeb425b0959121c9", result);
}

//! Одноблоковая коллизия для MD5 из http://marc-stevens.nl/research/md5-1block-collision/
TEST(TA_Programs, MD5_stevens_msg1_test)
{
	const std::string input_file = g_path + std::string("MD5_stevens_msg1.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("a24cafdb2df4d594e1eb3cf8394fddb3", result);
}

//! Одноблоковая коллизия для MD5 из http://marc-stevens.nl/research/md5-1block-collision/
TEST(TA_Programs, MD5_stevens_msg2_test)
{
	const std::string input_file = g_path + std::string("MD5_stevens_msg2.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("a24cafdb2df4d594e1eb3cf8394fddb3", result);
}

//! MD5-хеш от строки "md4"
TEST(TA_Programs, MD5_smd4_test)
{
	const std::string input_file = g_path + std::string("MD5_smd4.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("c93d3bf7a7c4afe94b64e30c2ce39f4f", result);
}

//! MD5-хеш от строки "" (пустая строка)
TEST(TA_Programs, MD5_s_test)
{
	const std::string input_file = g_path + std::string("MD5_s.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("d41d8cd98f00b204e9800998ecf8427e", result);
}

//! http://ru.wikipedia.org/wiki/MD4
//! MD5-хеш от строки "" (пустая строка)
TEST(TA_Programs, MD4_s_test)
{
	const std::string input_file = g_path + std::string("MD4_s.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 8);
	EXPECT_TRUE(ok);
	EXPECT_EQ("31d6cfe0d16ae931b73c59d7e0c089c0", result);
}

//! http://ru.wikipedia.org/wiki/Sha1
//! SHA-1 хеш от строки "" (пустая строка)
//! В отличие от MD4/MD5 читаем хеш не байтами, а 32-битными числами
TEST(TA_Programs, SHA_1_s_test)
{
	const std::string input_file = g_path + std::string("SHA-1_s.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 32);
	EXPECT_TRUE(ok);
	EXPECT_EQ("da39a3ee5e6b4b0d3255bfef95601890afd80709", result);
}

//! http://ru.wikipedia.org/wiki/SHA2
//! SHA-2 хеш от строки "" (пустая строка)
TEST(TA_Programs, SHA_2_s_test)
{
	const std::string input_file = g_path + std::string("SHA-2_s.alg");
	std::string result;
	const bool ok = ExecuteProgram(input_file, result, 32);
	EXPECT_TRUE(ok);
	EXPECT_EQ("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", result);
}
