#include "stdafx.h"
#include <TransalgParser/ParseError.h>
#include <TransalgParser/Parser.h>
#include <TransalgParser/Expression.h>
#include <TransalgParser/StatementIf.h>
#include <TransalgParser/StatementFor.h>
#include <TransalgParser/StatementVarDefinition.h>
#include <TransalgParser/StatementArrayDefinition.h>
#include <TransalgParser/StatementFunctionDefinition.h>
#include <TransalgParser/StatementReturn.h>
#include <TransalgParser/StatementTable.h>
#include <TransalgParser/StatementBlock.h>

#define TestExpression(cond, op_count) \
	EXPECT_TRUE(cond); \
	EXPECT_EQ(op_count, cond->GetOperandsCount());

#define TestExpressionOperand(op, type, arg_count, id, line) \
	EXPECT_EQ(arg_count, op.arg_count_); \
	EXPECT_EQ(type, op.type_); \
	EXPECT_EQ(line, op.line_); \
	EXPECT_EQ(id, op.id_); 

#define TestVarDefinition(ptr, attribute, data_type, id) \
	EXPECT_TRUE(ptr); \
	EXPECT_EQ(Transalg::stmtVarDefinition, ptr->GetStatementType()); \
	EXPECT_EQ(attribute, ptr->GetAttribute()); \
	EXPECT_EQ(data_type, ptr->GetDataType()); \
	EXPECT_EQ(id, ptr->GetId());

#define TestArrayDefinition(ptr, attribute, data_type, id, index_count, init_count) \
	EXPECT_TRUE(ptr); \
	EXPECT_EQ(Transalg::stmtArrayDefinition, ptr->GetStatementType()); \
	EXPECT_EQ(attribute, ptr->GetAttribute()); \
	EXPECT_EQ(data_type, ptr->GetDataType()); \
	EXPECT_EQ(id, ptr->GetId()); \
	EXPECT_EQ(index_count, ptr->GetIndexCount()); \
	EXPECT_EQ(init_count, ptr->GetInitValueCount());

#define TestFunctionDefinition(ptr, data_type, id, args_count) \
	EXPECT_TRUE(ptr); \
	EXPECT_EQ(Transalg::stmtFunctionDefinition, ptr->GetStatementType()); \
	EXPECT_EQ(data_type, ptr->GetDataType()); \
	EXPECT_EQ(id, ptr->GetId()); \
	EXPECT_EQ(args_count, ptr->GetArgsCount());

#define TestFunctionArgument(ptr, arg_num, data_type, id, indexes) \
	EXPECT_TRUE(ptr); \
	EXPECT_EQ(data_type, ptr->GetArgDataType(arg_num)); \
	EXPECT_EQ(id, ptr->GetArgId(arg_num)); \
	EXPECT_EQ(indexes, ptr->GetArgIndexes(arg_num));

template<typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec)
{
	out << "(";
	for(std::size_t i(0); i < vec.size(); ++i)
		out << (i==0?"":",") << vec[i];
	out << ")";
	return out;
}
