#include "TestMacros.h"

// Арифметические операции
TEST(ParserTests, ParseExpressionTest1)
{
	using namespace Transalg;
	const std::string text("a = b*c + (e/d - 5) % 3;");

	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// a b c * e d / 5 - 3 % + =
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 13);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "a", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "b", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeVariable, 0, "c", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "*", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeVariable, 0, "e", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeVariable, 0, "d", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeOperator, 2, "/", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeConst, 0, "5", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "-", 1);
	TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeConst, 0, "3", 1);
	TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeOperator, 2, "%", 1);
	TestExpressionOperand(ptr->GetOperand(11), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(12), Transalg::OperandTypeOperator, 2, "=", 1);
}

// Булевы операции
TEST(ParserTests, ParseExpressionTest2)
{
	using namespace Transalg;
	const std::string text("!(maj ^ regA[midA]) & (x | y);");

	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// maj midA regA ^ ! x y | &
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 9);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "maj", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "midA", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeArrayVariable, 1, "regA", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "^", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeOperator, 1, "!", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeVariable, 0, "x", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeVariable, 0, "y", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeOperator, 2, "|", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "&", 1);
}

// Вызов функции с пустыми аргументами
TEST(ParserTests, ParseExpressionTest3)
{
	using namespace Transalg;
	const std::string text("shiftReg();");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 1);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeFunction, 0, "shiftReg", 1);
}

// Вызов функции со списком аргументов
// Операция присваивания
TEST(ParserTests, ParseExpressionTest4)
{
	using namespace Transalg;
	const std::string text("maj = majority(regA[midA], regB[midB], regC[midC]);");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	// maj midA regA midB regB midC regC majority =
	TestExpression(ptr, 9);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "maj", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "midA", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeArrayVariable, 1, "regA", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "midB", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeArrayVariable, 1, "regB", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeVariable, 0, "midC", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeArrayVariable, 1, "regC", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeFunction, 3, "majority", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "=", 1);
}

// Операция конкатенации
// Умножение по модулю 2^n
TEST(ParserTests, ParseExpressionTest5)
{
	using namespace Transalg;
	const std::string text("mul(x || y, z, 24);");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	// x y || z 24 mul
	TestExpression(ptr, 6);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "||", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "z", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeConst, 0, "24", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeFunction, 3, "mul", 1);
}

// Операторы битового сдвига
TEST(ParserTests, ParseExpressionTest6)
{
	using namespace Transalg;
	const std::string text(
		"x = (reg[1] >> 1)^(reg[7] << 1);\n"
		"(reg[i + j] <<* 3) | (reg[1] >>* 1);\n"
		"reg[i][j] <<< 0x5;\n"
		"reg[0] >>> 0;"
	);
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(4, statements->GetStatementsCount());
	// x 1 reg 1 >> 7 reg 1 << ^ =
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
		TestExpression(ptr, 11);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeConst, 0, "1", 1);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeArrayVariable, 1, "reg", 1);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeConst, 0, "1", 1);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeOperator, 2, ">>", 1);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeConst, 0, "7", 1);
		TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeArrayVariable, 1, "reg", 1);
		TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeConst, 0, "1", 1);
		TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "<<", 1);
		TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeOperator, 2, "^", 1);
		TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeOperator, 2, "=", 1);
	}
	// i j + reg 3 *<< 1 reg 1 >>* |
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 1));
		TestExpression(ptr, 11);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 2);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "j", 2);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "+", 2);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeArrayVariable, 1, "reg", 2);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeConst, 0, "3", 2);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeOperator, 2, "<<*", 2);
		TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeConst, 0, "1", 2);
		TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeArrayVariable, 1, "reg", 2);
		TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeConst, 0, "1", 2);
		TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeOperator, 2, ">>*", 2);
		TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeOperator, 2, "|", 2);
	}
	// i j reg 0x5 <<<
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 2));
		TestExpression(ptr, 5);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 3);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "j", 3);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeArrayVariable, 2, "reg", 3);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeConst, 0, "0x5", 3);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeOperator, 2, "<<<", 3);
	}
	// 0 reg 0 >>>
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 3));
		TestExpression(ptr, 4);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeConst, 0, "0", 4);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeArrayVariable, 1, "reg", 4);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeConst, 0, "0", 4);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, ">>>", 4);
	}
}

// Операции сравнения
TEST(ParserTests, ParseExpressionTest7)
{
	using namespace Transalg;
	const std::string text(
		"pos = ((x > 0) & (y >= 0));\n"
		"neg = ((x <= 0) | (y < 0)) & z;\n"
		"(x == y) ^ (x != y);"
	);
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(3, statements->GetStatementsCount());
	// pos x 0 > y 0 >= & =
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
		TestExpression(ptr, 9);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "pos", 1);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeConst, 0, "0", 1);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, ">", 1);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeVariable, 0, "y", 1);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeConst, 0, "0", 1);
		TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeOperator, 2, ">=", 1);
		TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeOperator, 2, "&", 1);
		TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "=", 1);
	}
	// neg x 0 <= y 0 < | z & =
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 1));
		TestExpression(ptr, 11);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "neg", 2);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "x", 2);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeConst, 0, "0", 2);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "<=", 2);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeVariable, 0, "y", 2);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeConst, 0, "0", 2);
		TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeOperator, 2, "<", 2);
		TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeOperator, 2, "|", 2);
		TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeVariable, 0, "z", 2);
		TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeOperator, 2, "&", 2);
		TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeOperator, 2, "=", 2);
	}
	// x y == x y != ^
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 2));
		TestExpression(ptr, 7);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 3);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 3);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "==", 3);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "x", 3);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeVariable, 0, "y", 3);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeOperator, 2, "!=", 3);
		TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeOperator, 2, "^", 3);
	}
}

// Многомерные массивы
// Выражения в индексах массивов
TEST(ParserTests, ParseExpressionTest8)
{
	using namespace Transalg;
	const std::string text("output[i - 1][j] = mult[2][i + 1][0];");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// i 1 - j output 2 i 1 + 0 mult =
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 12);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "i", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "-", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "j", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeArrayVariable, 2, "output", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeConst, 0, "2", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeVariable, 0, "i", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeConst, 0, "0", 1);
	TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeArrayVariable, 3, "mult", 1);
	TestExpressionOperand(ptr->GetOperand(11), Transalg::OperandTypeOperator, 2, "=", 1);
}

// Вложеные функции (функция и массивы как аргументы других функций)
TEST(ParserTests, ParseExpressionTest9)
{
	using namespace Transalg;
	const std::string text("sum(sum(sum(a, I(b, c, d), 32), M[i + j], 32), t, 32);");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// a b c d I 32 sum i j + M 32 sum t 32 sum
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 16);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "a", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "b", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeVariable, 0, "c", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "d", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeFunction, 3, "I", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeConst, 0, "32", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeFunction, 3, "sum", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeVariable, 0, "i", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeVariable, 0, "j", 1);
	TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeArrayVariable, 1, "M", 1);
	TestExpressionOperand(ptr->GetOperand(11), Transalg::OperandTypeConst, 0, "32", 1);
	TestExpressionOperand(ptr->GetOperand(12), Transalg::OperandTypeFunction, 3, "sum", 1);
	TestExpressionOperand(ptr->GetOperand(13), Transalg::OperandTypeVariable, 0, "t", 1);
	TestExpressionOperand(ptr->GetOperand(14), Transalg::OperandTypeConst, 0, "32", 1);
	TestExpressionOperand(ptr->GetOperand(15), Transalg::OperandTypeFunction, 3, "sum", 1);
}

// Сложные скобочные структуры
// Шестнадцатиричные константы
// Операции битового сдвига
TEST(ParserTests, ParseExpressionTest10)
{
	using namespace Transalg;
	const std::string text("b + ((a + I(b, c, d) + M[1] + 0xd76aa478) <<< 12);");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// b a b c d I + 1 M + 0xd76aa478 + 12 <<< +
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 15);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "b", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "a", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeVariable, 0, "b", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "c", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeVariable, 0, "d", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeFunction, 3, "I", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeArrayVariable, 1, "M", 1);
	TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeConst, 0, "0xd76aa478", 1);
	TestExpressionOperand(ptr->GetOperand(11), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(12), Transalg::OperandTypeConst, 0, "12", 1);
	TestExpressionOperand(ptr->GetOperand(13), Transalg::OperandTypeOperator, 2, "<<<", 1);
	TestExpressionOperand(ptr->GetOperand(14), Transalg::OperandTypeOperator, 2, "+", 1);
}

// Сложные выражения в индексах массивов
TEST(ParserTests, ParseExpressionTest11)
{
	using namespace Transalg;
	const std::string text("m[get_index(x + 1, y * 2) - 1] - sum(x[z[i] + 1], y[j]) + 5;");
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(1, statements->GetStatementsCount());
	// x 1 + y 2 * get_index 1 - m i z 1 + x j y sum - 5 +
	ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
	TestExpression(ptr, 21);
	TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
	TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "y", 1);
	TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeConst, 0, "2", 1);
	TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeOperator, 2, "*", 1);
	TestExpressionOperand(ptr->GetOperand(6), Transalg::OperandTypeFunction, 2, "get_index", 1);
	TestExpressionOperand(ptr->GetOperand(7), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(8), Transalg::OperandTypeOperator, 2, "-", 1);
	TestExpressionOperand(ptr->GetOperand(9), Transalg::OperandTypeArrayVariable, 1, "m", 1);
	TestExpressionOperand(ptr->GetOperand(10), Transalg::OperandTypeVariable, 0, "i", 1);
	TestExpressionOperand(ptr->GetOperand(11), Transalg::OperandTypeArrayVariable, 1, "z", 1);
	TestExpressionOperand(ptr->GetOperand(12), Transalg::OperandTypeConst, 0, "1", 1);
	TestExpressionOperand(ptr->GetOperand(13), Transalg::OperandTypeOperator, 2, "+", 1);
	TestExpressionOperand(ptr->GetOperand(14), Transalg::OperandTypeArrayVariable, 1, "x", 1);
	TestExpressionOperand(ptr->GetOperand(15), Transalg::OperandTypeVariable, 0, "j", 1);
	TestExpressionOperand(ptr->GetOperand(16), Transalg::OperandTypeArrayVariable, 1, "y", 1);
	TestExpressionOperand(ptr->GetOperand(17), Transalg::OperandTypeFunction, 2, "sum", 1);
	TestExpressionOperand(ptr->GetOperand(18), Transalg::OperandTypeOperator, 2, "-", 1);
	TestExpressionOperand(ptr->GetOperand(19), Transalg::OperandTypeConst, 0, "5", 1);
	TestExpressionOperand(ptr->GetOperand(20), Transalg::OperandTypeOperator, 2, "+", 1);
}

// Выражения начинающиеся с унарных операторов, константы и скобок
TEST(ParserTests, ParseExpressionTest12)
{
	using namespace Transalg;
	const std::string text(
		"5 + x[i];\n"
		"-3 + x;\n"
		"((t-v)*x[i]);\n"
		"!(y&z);"
	);
	Parser parser;
	StatementBlockPtr statements = parser.ParseProgramText(text);
	EXPECT_EQ(4, statements->GetStatementsCount());
	// 5 i x +
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin()));
		TestExpression(ptr, 4);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeConst, 0, "5", 1);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "i", 1);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeArrayVariable, 1, "x", 1);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "+", 1);
	}
	// 3 - x +
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 1));
		TestExpression(ptr, 4);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeConst, 0, "3", 2);
		// fixme: '-' считает бинарным оператором, нужен ли унарный минус?
		// TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeOperator, 1, "-", 2);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeOperator, 2, "-", 2);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeVariable, 0, "x", 2);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "+", 2);
	}
	// t v - i x *
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 2));
		TestExpression(ptr, 6);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "t", 3);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "v", 3);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "-", 3);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeVariable, 0, "i", 3);
		TestExpressionOperand(ptr->GetOperand(4), Transalg::OperandTypeArrayVariable, 1, "x", 3);
		TestExpressionOperand(ptr->GetOperand(5), Transalg::OperandTypeOperator, 2, "*", 3);
	}
	// y z & !
	{
		ExpressionPtr ptr = boost::dynamic_pointer_cast<Expression>(*(statements->begin() + 3));
		TestExpression(ptr, 4);
		TestExpressionOperand(ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "y", 4);
		TestExpressionOperand(ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "z", 4);
		TestExpressionOperand(ptr->GetOperand(2), Transalg::OperandTypeOperator, 2, "&", 4);
		TestExpressionOperand(ptr->GetOperand(3), Transalg::OperandTypeOperator, 1, "!", 4);
	}
}
