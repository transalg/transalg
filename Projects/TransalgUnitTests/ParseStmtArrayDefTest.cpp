#include "TestMacros.h"

// Битовый массив без атрибутов.
// Длина массива задана константой, без инициализации.
TEST(ParserTests, ParseBitArrayDefTest1)
{
	using namespace Transalg;
	const std::string text(
		"bit vec[3];"
	);
	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(1, program_block->GetStatementsCount());
	StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*(program_block->begin()));
	TestArrayDefinition(ptr, Transalg::AttributeNone, Transalg::dtBit, "vec", 1, 0);
	EXPECT_EQ(3, ptr->GetIndex(0));
	EXPECT_EQ(0, ptr->GetInitExpression().get());
}

// Размер массива задан именованной константой.
// Инициализация массива константами.
TEST(ParserTests, ParseBitArrayDefTest2)
{
	using namespace Transalg;
	const std::string text(
		"define len 3;"
		"bit reg[len] = {1, 0, 1};"
	);

	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(1, program_block->GetStatementsCount());

	StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*(program_block->begin()));
	TestArrayDefinition(ptr, Transalg::AttributeNone, Transalg::dtBit, "reg", 1, 3);
	EXPECT_EQ(3, ptr->GetIndex(0));
	EXPECT_EQ(1, ptr->GetInitValue(0));
	EXPECT_EQ(0, ptr->GetInitValue(1));
	EXPECT_EQ(1, ptr->GetInitValue(2));
	EXPECT_EQ(0, ptr->GetInitExpression().get());
}

// Различные атрибуты.
// Инициализация массива константой.
TEST(ParserTests, ParseBitArrayDefTest3)
{
	using namespace Transalg;
	const std::string text(
		"define len 3;"
		"__in bit A[len] = 0xf3;"
		"__mem bit B[len] = {0, 1, 1};"
		"__out bit C[len];"
	);

	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(3, program_block->GetStatementsCount());
	StatementContainer::const_iterator stmt_it = program_block->begin();
	//! __in
	{
		StatementArrayDefinitionPtr in_ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*stmt_it);
		TestArrayDefinition(in_ptr, Transalg::AttributeIn, Transalg::dtBit, "A", 1, 0);
		EXPECT_EQ(3, in_ptr->GetIndex(0));
		ExpressionPtr in_expr_ptr = in_ptr->GetInitExpression();
		TestExpression(in_expr_ptr, 1);
		TestExpressionOperand(in_expr_ptr->GetOperand(0), Transalg::OperandTypeConst, 0, "0xf3", 1);
	}

	//! __mem
	{
		StatementArrayDefinitionPtr mem_ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*++stmt_it);
		TestArrayDefinition(mem_ptr, Transalg::AttributeMem, Transalg::dtBit, "B", 1, 3);
		EXPECT_EQ(3, mem_ptr->GetIndex(0));
		EXPECT_EQ(0, mem_ptr->GetInitValue(0));
		EXPECT_EQ(1, mem_ptr->GetInitValue(1));
		EXPECT_EQ(1, mem_ptr->GetInitValue(2));
		EXPECT_EQ(0, mem_ptr->GetInitExpression().get());
	}

	//! __out
	{
		StatementArrayDefinitionPtr out_ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*++stmt_it);
		TestArrayDefinition(out_ptr, Transalg::AttributeOut, Transalg::dtBit, "C", 1, 0);
		EXPECT_EQ(3, out_ptr->GetIndex(0));
		EXPECT_EQ(0, out_ptr->GetInitExpression().get());
	}
}

// Объявление двумерного целочисленного массива.
// Инициализация целочисленного массива списком значений.
TEST(ParserTests, ParseIntArrayDefTest1)
{
	using namespace Transalg;
	const std::string text(
		"define len 3;"
		"int X[5][4];"
		"int Y[len] = {0, 5, 10};"
	);

	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(2, program_block->GetStatementsCount());

	StatementContainer::const_iterator stmt_it = program_block->begin();
	{
		StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*stmt_it);
		TestArrayDefinition(ptr, Transalg::AttributeNone, Transalg::dtInt, "X", 2, 0);
		EXPECT_EQ(5, ptr->GetIndex(0));
		EXPECT_EQ(4, ptr->GetIndex(1));
		EXPECT_EQ(0, ptr->GetInitExpression().get());
	}
	{
		StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*++stmt_it);
		TestArrayDefinition(ptr, Transalg::AttributeNone, Transalg::dtInt, "Y", 1, 3);
		EXPECT_EQ(3, ptr->GetIndex(0));
		EXPECT_EQ(0, ptr->GetInitValue(0));
		EXPECT_EQ(5, ptr->GetInitValue(1));
		EXPECT_EQ(10, ptr->GetInitValue(2));
		EXPECT_EQ(0, ptr->GetInitExpression().get());
	}
}

// Инициализация многомерного целочисленного массива
TEST(ParserTests, ParseIntArrayDefTest2)
{
	using namespace Transalg;
	const std::string text(
		"define len 2;"
		"int X[3][len] = {{1, 2}, {3, 4}, {5, 6}};"
	);

	Parser parser;
	StatementBlockPtr program = parser.ParseProgramText(text);
	EXPECT_EQ(1, program->GetStatementsCount());
	StatementArrayDefinitionPtr ptr = boost::dynamic_pointer_cast<StatementArrayDefinition>(*(program->begin()));
	TestArrayDefinition(ptr, Transalg::AttributeNone, Transalg::dtInt, "X", 2, 6);
	EXPECT_EQ(3, ptr->GetIndex(0));
	EXPECT_EQ(2, ptr->GetIndex(1));
	for(int i = 0; i < 6; ++i)
		EXPECT_EQ(i + 1, ptr->GetInitValue(i));
	EXPECT_EQ(0, ptr->GetInitExpression().get());
}
