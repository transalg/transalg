TEMPLATE = lib
CONFIG += staticlib
TARGET = EspressoLib

DEFINES += __STDC__
DEFINES += _CRT_SECURE_NO_WARNINGS

include("../../Mk/Common/common.pri")

CONFIG(debug, debug|release) {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$DEBUG_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
} else {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$RELEASE_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
}

#message("DESTDIR: $$DESTDIR")
#message("OBJECTS_DIR: $$OBJECTS_DIR")

HEADERS += \
	copyright.h \
	espresso.h \
	main.h \
	mincov.h \
	mincov_int.h \
	port.h \
	signature.h \
	sparse.h \
	sparse_int.h \
	utility.h

SOURCES += \
	black_white.c \
	canonical.c \
	cofactor.c \
	cols.c \
	compl.c \
	contain.c \
	cpu_time.c \
	cubestr.c \
	cvrin.c \
	cvrm.c \
	cvrmisc.c \
	cvrout.c \
	dominate.c \
	equiv.c \
	espresso.c \
	essen.c \
	essentiality.c \
	exact.c \
	expand.c \
	gasp.c \
	gimpel.c \
	globals.c \
	hack.c \
	indep.c \
	irred.c \
	map.c \
	matrix.c \
	mincov.c \
	opo.c \
	pair.c \
	part.c \
	primes.c \
	prtime.c \
	reduce.c \
	rows.c \
	set.c \
	setc.c \
	sharp.c \
	sigma.c \
	signature.c \
	signature_exact.c \
	sminterf.c \
	solution.c \
	sparse.c \
	unate.c \
	util_signature.c \
	verify.c

