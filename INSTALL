
This text describes how to build Transalg from source files in Linux 
environment. Transalg uses QMake build system and several libraries 
from the Boost project. Modern Linux distributions like Debian or 
RedHat (and their numerous offsprings, such as Ubuntu, Fedora, CentOS, 
etc.) provide QMake and Boost packages via their package management 
systems: APT and YUM. This manual contains typical commands necessary 
to install Transalg dependencies with APT and YUM.

We do not cover installation instructions for other families of Linux 
distributions, FreeBSD or MacOS. These instructions have been tested 
for Ubuntu 14.10 x86/x64 and Fedora Workstation x64 21-5

*** A few short notes. ***

In this manual by ">" we mean "run the following line at console".

"Redhat>" - instructions for RedHat-based distributions only

"Debian>" - instructions for Debian-based distributions only

*********************************************************************
* Step -1. Update package manager repositories ***
*********************************************************************

It is always a good idea to update your distribution before installing
something new:

Redhat> yum update

Debian> apt-get update && apt-get upgrade

*********************************************************************
* Step 0. Install git ***
*********************************************************************

To be able to get files from GIT repositories conveniently you need to
install GIT tools.

RedHat> yum install git

Debian> apt-get install git

*********************************************************************
* Step 1. Install qmake and g++ (if you don't have them * already) *
*********************************************************************

RedHat> apt-get install qt-devel gcc-c++

Debian> apt-get install qt4-qmake g++

*********************************************************************
* Step 2. Install necessary Boost development libraries *
*********************************************************************

Transalg uses Boost Threads, Boost System, and Boost Program Options
libraries from the Boost collection. We need to get development packages
for Boost (i.e. with development headers). If your distribution does 
not provide Boost libraries you'll have to download, compile and install 
them by yourself. Otherwise:

RedHat> yum install boost-devel

Debian> apt-get install libboost-system-dev libboost-thread-dev libboost-program-options-dev

*********************************************************************
* Step 3. Get Transalg source code *
*********************************************************************

The fastest way to get up-to-date Transalg sources is to check it out
from GIT repository.

> git clone git://gitorious.org/transalg/transalg.git
> cd transalg

*********************************************************************
* Step 4. Build Transalg *
*********************************************************************

> cd Projects
(Assuming we're now in "transalg/Projects" folder.)

> qmake

Debian> qmake

Redhat> qmake-qt5

> make

If compilation was successful, you will find freshly compiled Transalg
binaries in folders:

transalg/Bin/Debug

transalg/Bin/Release
