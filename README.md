## Transalg: a tool for propositional encoding of cryptographic functions

Transalg was designed for automatic construction of propositional encodings of discrete functions based on their algorithmic descriptions (algorithms). Such algorithms should be written as programs in a procedural programming language. Transalg uses domain specific language named TA language for this purpose. 
In Transalg propositional encoding is performed via symbolic execution of a TA-program (a program written in TA-language) which implements the corresponding algorithm. The result of a symbolic execution is a system of Boolean equations over the set of variables encoding the input of the algorithm and the set of additional variables encoding the calculation process. The main format, in which the resulting system of Boolean equations can be presented, is CNF. Additionally, it is possible to form a result in AIG (and-inverter graph) format. 
Main application of Transalg is the propositional encoding of cryptographic functions for the purpose of algebraic cryptanalysis.

## Installation

This manual describes how to build Transalg from source files in Linux 
environment. 

Transalg uses QMake build system and several libraries 
from the Boost project. Modern Linux distributions like Debian or 
RedHat (and their numerous offsprings, such as Ubuntu, Fedora, CentOS, 
etc.) provide QMake and Boost packages via their package management 
systems: APT and YUM. This manual contains typical commands necessary 
to install Transalg dependencies with APT and YUM.

We do not cover installation instructions for other families of Linux 
distributions, FreeBSD or MacOS. These instructions have been tested 
for Ubuntu 14.10 x86/x64 and Fedora Workstation x64 21-5

>In this manual by ">" we mean "run the following line at console".
>"Redhat>" - instructions for RedHat-based distributions only
>"Debian>" - instructions for Debian-based distributions only

**Step 1. Update package manager repositories**

It is always a good idea to update your distribution before installing
something new:

    Redhat> yum update
    Debian> apt-get update && apt-get upgrade

**Step 2. Install git**

To be able to get files from GIT repositories conveniently you need to
install GIT tools.

    RedHat> yum install git
    Debian> apt-get install git

**Step 2. Install qmake and g++**

    RedHat> apt-get install qt-devel gcc-c++
    Debian> apt-get install qt4-qmake g++

**Step 3. Install necessary Boost development libraries**

Transalg uses Boost Threads, Boost System, and Boost Program Options
libraries from the Boost collection. We need to get development packages
for Boost (i.e. with development headers). 

If your distribution does 
not provide Boost libraries you'll have to download, compile and install 
them by yourself. Otherwise:

    RedHat> yum install boost-devel
    Debian> apt-get install libboost-system-dev libboost-thread-dev libboost-program-options-dev

**Step 3. Get Transalg source code**

The fastest way to get up-to-date Transalg sources is to check it out
from GIT repository.

    > git clone git://gitorious.org/transalg/transalg.git
    > cd transalg

**Step 4. Build Transalg**

    > cd Projects
Assuming we're now in "transalg/Projects" folder. 

Run qmake and make:

    Debian> qmake && make
    Redhat> qmake-qt5 && make

If compilation was successful, you will find freshly compiled Transalg
binaries in folders:

transalg/Bin/Debug

transalg/Bin/Release


## Usage

### Main available options
1.  **-h [ --help ]** shows full list of available options.
2.  **-i [ --input ]** sets the input file – the name of the text file containing the TA program. 
3.  **-o [ --output ]** sets the output file – the name of the file that will contain the result of symbolic execution of the TA-program.
4.  **-f [ --format ]** sets the output format {cnf, ls, aig}.

The default option is **-f cnf**. In this case, the result is written to CNF (a text file in DIMACS format).
Using option **-f ls** the result can be output as a set of Boolean formulas, written in postfix form. This output format directly corresponds to the internal representation of the propositional encoding within Transalg, so it can be useful for debugging and for understanding the process of creating new code variables.
Option **-f aig** allows to write the result in AIG format (AIGER text file).

5. **--no-optimize** disables the minimization of CNF using Espresso.
6. **--verbose** turns on the output of additional statistics.

**Usage example:** 

    ./Transalg -i myfunction.alg -o myfunction_encoding.cnf

### TA language
In Transalg the input data is a text file containing a program written in a specialized programming language (TA-language). This program defines an algorithm which calculates some discrete function and called TA-program.

There are two main data types in TA language: *bit* and *int*. 
The *bit* data type is designed to work with symbolic data – *bit*-type variables are encoded using Boolean variables. Transalg implements the symbolic execution only on variables of *bit* type. 
The variables defining the input and output of the algorithm are always defined as *bit* variables, since they are included in a set of variables of symbolic execution. Additionally, there are special attributes __in (for declaring input variables) and __out (for output variables) which are necessary to create the corresponding code variables at the initial step of the symbolic execution.

	__in bit input[64];
	__out bit output[32];

In this example there are declared two *bit* vectors: first one is a 64-bit vector of Boolean variables, which represents the input of the algorithm (64-bit word or number), and second one is a 32-bit vector of Boolean variables that encode the output.

The *int* data type is designed to work with known numeric data (for example, loop counters or known algorithm parameters). 

A program in TA language consists of a set of procedures/functions, global variables and constants. The variables encoding the input and output of the algorithm are always declared as global, i.e. outside the body of functions and procedures. The symbolic execution starts with function main, which is a starting point (entry point) and essential part any TA program (similarly with the C language).

    void main()
    {
        // some operations
    }

The body of any function in TA-program is a list of operators. The TA language supports the basic operators of procedural programming languages:

- **variable declaration**
    ```
    bit x;
    int i = 0;
    ```
- **integer constant declaration** 
	```
    define i 5;
    ```
- **assignment operator** 
	```
    x = reg[i];
    ```
- **loop operator**
	```
    for (int i = 0; i < n; i = i + 1)
	{
	    // some operations
	}
	```
- **conditional operator**
	```
    bit condition;
	...
	if (condition)
	{
	    // some operations
	}
	else
	{
	    // some operations
	}
	```
-  **procedure/function call**
	```
    m = majority(x, y, z);
    ```

TA language supports basic arithmetic and logical operations. 

Bitwise logical operations: conjunction (&), disjunction (|), XOR operation or addition modulo 2 (^) and negation (!).

Integer operations: addition, subtraction, multiplication.

In addition, for vectors of type bit there are special build-in functions:
- sum(x, y, m) calculates the integer sum of x and y and returns the first m bits of this value.
- mul(x, y, m) calculates the integer product of x and y and returns the first m bits of this value.


### Examples
1. The linear feedback shift register (LFSR) is one of the common basic elements used in modern stream ciphers. Below is an example of a TA-program that implements the LFSR with the feedback function f (x) = x which generates 128 bits of the keystream.
    ```
	define len 128;
	__in bit reg[19];
	__out bit result[len];
	bit shift_rslos()
	{
		bit x = reg[18];
		bit y = reg[18] ^ reg[17] ^ reg[16] ^ reg[13];
		for(int j = 18; j > 0; j = j - 1)
		{
			reg[j] = reg[j - 1];
		}
		reg[0] = y;
		return x;
	}
	void main()
	{
		for(int i = 0; i < len; i = i + 1)
		{
			result[i] = shift_rslos();
		}
	}
    ```
2. This example demonstrates the application of conditional operators, which is a feature of the A5 /1 keystream generator:
    ```
	void main()
	{
		int midA = 8;
		int midB = 10;
		int midC = 10;
		bit maj;
		for(int i = 0; i < len; i= i + 1)
		{
			maj = majority(regA[midA],regB[midB],regC[midC]);
			if(!(maj^regA[midA])) shift_rslosA();
			if(!(maj^regB[midB])) shift_rslosB();
			if(!(maj^regC[midC])) shift_rslosC();                           
			result[i] = regA[18]^regB[21]^regC[22];
		}
	}
    ```
    Full example of the implementation of A5/1 including additional functions (majority, shift_rslosA, shift_rslosB, shift_rslosC) can be found at 
    https://gitlab.com/satencodings/satencodings/blob/master/A5_1/A5_1.alg

3. In addition to the register with linear feedback (LFSR) the Grain keystream generator uses a register with nonlinear feedback (NFSR). However, the feedback function is too complicated for effective processing by one formula. This example demonstrates how to break large logical expressions into parts by using additional variables with attribute __mem.
    ```
	void NFSR_shift()
	{
	__mem bit y1 = NFSR[52]&NFSR[45]&NFSR[37]&NFSR[33]&NFSR[28]&NFSR[21] ^ NFSR[33]&NFSR[28]&NFSR[21] ^ NFSR[37]&NFSR[33] ^ NFSR[52] ^ NFSR[45] ^ NFSR[37] ^ NFSR[33] ^ NFSR[28] ^ NFSR[21];
	__mem bit y2 = NFSR[33]&NFSR[28]&NFSR[21]&NFSR[15]&NFSR[9] ^ NFSR[15]&NFSR[9] ^ NFSR[14] ^ NFSR[9];
	__mem bit y3 = NFSR[63]&NFSR[60]&NFSR[52]&NFSR[45]&NFSR[37] ^ NFSR[60]&NFSR[52]&NFSR[45] ^ NFSR[60]&NFSR[52]&NFSR[37]&NFSR[33];
	__mem bit y4 = NFSR[63]&NFSR[60]&NFSR[21]&NFSR[15] ^ NFSR[63]&NFSR[60] ^ NFSR[62] ^ NFSR[60];
	__mem bit y5 =  NFSR[0] ^ NFSR[63]&NFSR[45]&NFSR[28]&NFSR[9];
	__mem bit y = y1 ^ y2 ^ y3 ^ y4 ^ y5;
	for(int j = 0; j < 79; j = j + 1)
	{
		NFSR[j] = NFSR[j + 1];
	}
	NFSR[79] = y ^ LFSR[0];
	}
    ```
    Full example of the implementation of the Grain keystream generator (version 1) can be found at 
    https://gitlab.com/satencodings/satencodings/blob/master/Grain/Grain_no_init_ver1.alg

4. In MD4 hash function the operations of integer addition and bitwise operations applied to integers are actively used. Each integer in the program is represented by a vector of bit type. The examples of the implementation of round functions used in MD4 are presented below.
    ```
	bit FF(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(a, F(b, c, d), 32), M, 32);
		return (a <<< s);
	}
	bit GG(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(sum(a, G(b, c, d), 32), M, 32), 0x5A827999, 32);
		return (a <<< s);
	}
	bit HH(bit a[32], bit b[32], bit c[32], bit d[32], bit M[32], int s)
	{
		a = sum(sum(sum(a, H(b, c, d), 32), M, 32), 0x6ED9EBA1, 32);
		return (a <<< s);
	}
	```    
    Full example of the implementation of MD4 can be found at https://gitlab.com/satencodings/satencodings/blob/master/MD4/MD4.alg

More examples of TA-programs can be found at https://gitlab.com/satencodings/satencodings.git 

## Citation

Transalg can be cited as follows:
```
@inproceedings{DBLP:conf/ecai/OtpuschennikovS16,
  author    = {Ilya Otpuschennikov and
               Alexander Semenov and
               Irina Gribanova and
               Oleg Zaikin and
               Stepan Kochemazov},
  title     = {Encoding Cryptographic Functions to {SAT} Using {TRANSALG} System},
  booktitle = {22nd European Conference on Artificial Intelligence - {ECAI} 2016},
  series    = {Frontiers in Artificial Intelligence and Applications},
  volume    = {285},
  publisher = {{IOS} Press},
  pages     = {1594--1595},
  year      = {2016}
}
```